window.addEventListener('load', main);

function main() {
  addStickyHeader();
  createMenu();
  showModalImage();
}

function addStickyHeader() {
  const headers = document.getElementsByTagName('header');
  if (headers) {
    const header = headers[0];
    header.classList.add('stickyTop');
    const parentBlock = header.parentElement;
    parentBlock.classList.add('stickyTop');
  }
}

class NodeElement {
  constructor(
    options = {
      id,
      index,
      level,
      innerText,
      parentEl,
    },
  ) {
    this.id = options.id;
    this.index = options.index;
    this.uuid = this.generateUUID();

    this.level = options.level;
    this.innerText = options.innerText;

    const parent = options.parentEl ? options.parentEl : '#menu';

    this.parent = parent;
    this.children = [];
  }

  generateUUID() {
    return `${this.index}-${this.id}`;
  }

  addChild(child) {
    this.children.push(child);
  }
}

/**
 * Create a new menu with sorted headings.
 */
function createMenu() {
  const menuElement = document.getElementById('menu');
  const nodeElmts = getPageHeadings();

  const ul = document.createElement('ul');
  for (let i = 0; i < nodeElmts.length; i++) {
    const nodeElmt = nodeElmts[i];
    const liElmt = createLi(nodeElmt);
    ul.appendChild(liElmt);
  }
  menuElement.appendChild(ul);
}

/**
 * @returns NodeElement
 */
function getPageHeadings() {
  const headingElements = document.querySelectorAll('h1, h2, h3, h4, h5, h6');
  const tmpElements = [];

  for (const headingElement of headingElements) {
    if (headingElement.parentElement === document.querySelector('header')) {
      continue;
    }
    tmpElements.push(headingElement);
  }

  const acc = [];
  let tmpParent = null;

  for (let idx = 0; idx < tmpElements.length; idx++) {
    const CURRENT_ELEMENT = tmpElements[idx];
    const PREVIOUS_ELEMENT = tmpElements[idx - 1];

    const CURRENT_ID = CURRENT_ELEMENT.getAttribute('id');
    const CURRENT_LEVEL = getLevel(CURRENT_ELEMENT);

    if (idx === 0) {
      tmpParent = new NodeElement({
        id: CURRENT_ID,
        index: idx,
        level: CURRENT_LEVEL,
        innerText: CURRENT_ELEMENT.innerText,
      });
      acc.push(tmpParent);
      continue;
    }

    const PREVIOUS_LEVEL = getLevel(PREVIOUS_ELEMENT);

    if (CURRENT_LEVEL < PREVIOUS_LEVEL) {
      // go up in the tree
      const parentElmt = findByLevel(acc, CURRENT_LEVEL);
      if (parentElmt !== undefined) {
        const newElmt = new NodeElement({
          id: CURRENT_ID,
          index: idx,
          level: CURRENT_LEVEL,
          innerText: CURRENT_ELEMENT.innerText,
          parentEl: parentElmt.uuid
        });

        parentElmt.children.push(newElmt);
        tmpParent = newElmt;
      } else {
        const newElmt = new NodeElement({
          id: CURRENT_ID,
          index: idx,
          level: CURRENT_LEVEL,
          innerText: CURRENT_ELEMENT.innerText
        });
        tmpParent = newElmt;
        acc.push(newElmt);
      }
      continue;
    }

    if (CURRENT_LEVEL === PREVIOUS_LEVEL) {
      const parentElmt = findByLevel(acc, CURRENT_LEVEL);
      let newNode;
      if (parentElmt === undefined) {
        newNode = new NodeElement({
          id: CURRENT_ID,
          index: idx,
          level: CURRENT_LEVEL,
          innerText: CURRENT_ELEMENT.innerText
        })
        acc.push(newNode);
      } else {
        newNode = new NodeElement({
          id: CURRENT_ID,
          index: idx,
          level: CURRENT_LEVEL,
          innerText: CURRENT_ELEMENT.innerText,
          parentEl: parentElmt.uuid,
        })
        parentElmt.addChild(newNode);
      }
      tmpParent = newNode;
      continue;
    }

    if (CURRENT_LEVEL > PREVIOUS_LEVEL) {
      const child = new NodeElement({
        id: CURRENT_ID,
        index: idx,
        level: CURRENT_LEVEL,
        innerText: CURRENT_ELEMENT.innerText,
        parentEl: tmpParent.uuid,
      });

      tmpParent.addChild(
        child,
      );

      tmpParent = child;
      continue;
    }
  }

  return acc;
}

/**
 * Create a new list item.
 *
 * @param {NodeElement}
 */
function createLi(nodeElmt) {
  const li = document.createElement('li');
  li.setAttribute('id', `nav-${nodeElmt.id}`);
  li.appendChild(createLink(nodeElmt));

  if (nodeElmt.children.length > 0) {
    const childUl = document.createElement('ul');
    for (let i = 0; i < nodeElmt.children.length; i++) {
      const childElmt = nodeElmt.children[i];
      const li = createLi(childElmt);
      childUl.appendChild(li);
    }
    li.appendChild(childUl);
  }
  return li;
}

/**
 * Create a new link element.
 *
 * @param {NodeElement}
 */
function createLink(nodeElmt) {
  const link = document.createElement('a');
  link.setAttribute('href', `#${nodeElmt.id}`);
  link.innerText = nodeElmt.innerText;
  const header = document.getElementsByTagName('header')[0];

  // avoid anchor to be under the header
  const headerClass = [...header.classList];
  if (headerClass.includes('stickyTop')){
    const targetElmt = document.getElementById(nodeElmt.id);
    targetElmt.style.scrollMarginTop = `${header.offsetHeight}px`;
  }
  return link;
}

/**
 * Get the level of the heading.
 *
 * @param {HTMLElement} element Reference to the element to get the level from
 * @returns {Number} The heading level of the element
 */
function getLevel(element) {
  return parseInt(element.tagName.replace(/H/, ''));
}

/**
 * find a nodeElement by it's level
 */
function findByLevel(nodeArray, level) {
  // copy the given arr to avoid mutating it
  const arr = [...nodeArray];
  for (let i = arr.length - 1; i >= 0; i--) {
    const nodeElmt = arr[i];
    const hasOneHigherChild = nodeElmt.children.some(child => child.level < level);
    if (nodeElmt.level < level && hasOneHigherChild === false) return nodeElmt;
    else {
      return findByLevel(nodeElmt.children, level);
    }
  }
}

const sanitize = (className) => (className.charAt(0) === '.' ? className.substr(1) : className);
const getClassNames = (element) => (element.className ? element.className.split(' ') : []);

const modifyClass = (element, className, remove) => {
  className = sanitize(className);
  const classNames = getClassNames(element);
  const index = classNames.indexOf(className);
  if (remove === true) {
    if (index > -1) classNames.splice(index, 1);
  } else if (index === -1) classNames.push(className);
  element.className = classNames.join(' ');
};
const addClass = (element, className) => modifyClass(element, className);
const removeClass = (element, className) => modifyClass(element, className, true);

function showModalImage() {
  const medias = document.querySelectorAll('.content-media');
  let modalOpened = false;
  let mutate = false;

  for (const media of medias) {
    const modal = media.querySelector('.content-media__modal');
    const modalImage = modal.querySelector('.content-media__modal-img');
    const modalCaption = modal.querySelector('.content-media__modal-caption');
    const image = media.querySelector('.content-media__img img');
    const parentIsLink = media.closest('a');

    image.addEventListener('click', (event) => {
      if (parentIsLink) return;
      if (mutate || !modal || !modalImage) {
        event.preventDefault();
        return;
      }

      if (!modalOpened) {
        if (modal) {
          mutate = true;

          addClass(modal, '.on');

          window.setTimeout(() => {
            mutate = false;
            modalOpened = true;
          }, 500);
        }
      }
    });

    modal.addEventListener('click', (event) => {
      if (parentIsLink) return;
      if (mutate || !modal || !modalImage) {
        event.preventDefault();
        return;
      }

      if (modalOpened) {
        mutate = true;

        addClass(modal, '.out');
        addClass(modalImage, '.out');
        if (modalCaption) addClass(modalCaption, '.out');

        window.setTimeout(() => {
          removeClass(modal, '.out');
          removeClass(modal, '.on');
          removeClass(modalImage, '.out');
          if (modalCaption) removeClass(modalCaption, '.out');

          mutate = false;
          modalOpened = false;
        }, 500);
      }
    });
  }
}
