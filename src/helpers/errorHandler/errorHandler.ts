import { LoggerInterface } from '../logger/logger';

export interface ErrorHandlerInterface {
  execute(error: any): void;
}

export class ErrorHandler implements ErrorHandlerInterface {
  private logger: LoggerInterface;

  constructor(logger: LoggerInterface) {
    this.logger = logger;
  }

  execute(error: any): void {
    // check if is an Error object
    if (error && error.stack && error.message) {
      // add the stacktrace if in dev env
      if (process.env.NODE_ENV === 'dev') {
        this.logger.error(error.message, error.stack);
        return;
      }

      this.logger.error(error.message);
      return;
    }
    this.logger.error(error);
  }
}
