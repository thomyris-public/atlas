import { Logger } from '../logger/logger';
import { ErrorHandler } from './errorHandler';

describe('ErrorHandler', () => {
  const logger = new Logger();
  const errorHandler = new ErrorHandler(logger);
  const spyLogError = jest.spyOn(logger, 'error');

  const spyError = jest.spyOn(console, 'error');
  const spyInfo = jest.spyOn(console, 'info');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  /* eslint @typescript-eslint/no-empty-function: "off" */
  beforeAll(() => {
    // avoid geting the log message in the terminal comment to see log in console
    spyError.mockImplementation(() => {});
    spyInfo.mockImplementation(() => {});
  });

  afterAll(() => {
    process.env.NODE_ENV = undefined; // Restore old environment
  });

  it('Should call logger.error if it get an error object', () => {
    // arrange
    const error = new Error('test');
    // act
    errorHandler.execute(error);
    // assert
    expect(spyLogError).toHaveBeenCalledTimes(1);
    expect(spyLogError).toHaveBeenCalledWith(error.message);
  });

  it('Should call logger.error if it get an error object with the stack because dev env', () => {
    // arrange
    process.env.NODE_ENV = 'dev';
    const error = new Error('test');
    error.stack = 'line: 0';
    // act
    errorHandler.execute(error);
    // assert
    expect(spyLogError).toHaveBeenCalledTimes(1);
    expect(spyLogError).toHaveBeenCalledWith(error.message, error.stack);
  });

  it('Should call logger.error if it get smth else', () => {
    // arrange
    const error = ['test'];
    // act
    errorHandler.execute(error);
    // assert
    expect(spyLogError).toHaveBeenCalledTimes(1);
    expect(spyLogError).toHaveBeenCalledWith(error);
  });
});
