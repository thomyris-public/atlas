// generate infoFile.json in assets folder

const promises = require('fs').promises;// eslint-disable-line @typescript-eslint/no-var-requires

async function infoFile(){
  const path = 'src/assets/info.json';
  const content = JSON.stringify({name: process.env.npm_package_name, version: process.env.npm_package_version});
  promises.writeFile(path, content);
}

infoFile();