import { promises, existsSync } from 'fs';

import { Conf } from './conf.model';

export interface GetConfInterface {
  get(): Promise<Conf>;
}
export class GetConf {
  async get(): Promise<Conf> {
    // get the npm variable
    const path: string = process.argv.slice(2)[0];

    // check if the file exist
    if (existsSync(path)) {
      const confString: string = await promises.readFile(path, 'utf-8');
      const conf: Conf = JSON.parse(confString);
      this.checkConf(conf);
      return conf;
    } else {
      throw new Error('Configuration file not found.');
    }
  }

  // check if the conf file is properly writen
  private checkConf(conf: Conf): void {
    if (!conf.input) throw new Error('Configuration file need an input property');
    if (!conf.output) throw new Error('Configuration file need an output property');
    if (typeof conf.input !== 'string') throw new Error('Property input must be a string');
    if (typeof conf.output !== 'string') throw new Error('Property output must be a string');
  }
}
