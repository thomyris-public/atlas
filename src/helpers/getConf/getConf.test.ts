import * as fs from 'fs';

import { GetConf } from './getConf';
import { Conf } from './conf.model';

describe('GetConf', () => {
  const getConf = new GetConf();
  const spyExist = jest.spyOn(fs, 'existsSync');
  const spyRead = jest.spyOn(fs.promises, 'readFile');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  describe('#get', () => {
    it('Should return a Conf model', async () => {
      // arrange
      const conf: Conf = { input: 'input', output: 'output' };
      const file = JSON.stringify(conf);
      spyExist.mockReturnValueOnce(true);
      spyRead.mockResolvedValueOnce(file);
      // act
      const result: Conf = await getConf.get();
      // assert
      expect(result).toStrictEqual(conf);
    });

    it('Should throw an error if the file is not found', async () => {
      // arrange
      spyExist.mockReturnValueOnce(false);
      // act
      let result: Conf | any;
      try {
        result = await getConf.get();
      } catch (error) {
        result = error;
      }

      // assert
      expect(result).toStrictEqual(new Error('Configuration file not found.'));
    });

    it('Should throw an error if input property is missing', async () => {
      // arrange
      const conf = { output: 'output' };
      const file = JSON.stringify(conf);
      spyExist.mockReturnValueOnce(true);
      spyRead.mockResolvedValueOnce(file);
      // act
      let result: Conf | any;
      try {
        result = await getConf.get();
      } catch (error) {
        result = error;
      }

      // assert
      expect(result).toStrictEqual(new Error('Configuration file need an input property'));
    });

    it('Should throw an error if input property is not a string', async () => {
      // arrange
      const conf = { input: 1, output: 'output' };
      const file = JSON.stringify(conf);
      spyExist.mockReturnValueOnce(true);
      spyRead.mockResolvedValueOnce(file);
      // act
      let result: Conf | any;
      try {
        result = await getConf.get();
      } catch (error) {
        result = error;
      }

      // assert
      expect(result).toStrictEqual(new Error('Property input must be a string'));
    });

    it('Should throw an error if output property is missing', async () => {
      // arrange
      const conf = { input: 'input' };
      const file = JSON.stringify(conf);
      spyExist.mockReturnValueOnce(true);
      spyRead.mockResolvedValueOnce(file);
      // act
      let result: Conf | any;
      try {
        result = await getConf.get();
      } catch (error) {
        result = error;
      }

      // assert
      expect(result).toStrictEqual(new Error('Configuration file need an output property'));
    });

    it('Should throw an error if output property is not a string', async () => {
      // arrange
      const conf = { input: 'input', output: 1 };
      const file = JSON.stringify(conf);
      spyExist.mockReturnValueOnce(true);
      spyRead.mockResolvedValueOnce(file);
      // act
      let result: Conf | any;
      try {
        result = await getConf.get();
      } catch (error) {
        result = error;
      }

      // assert
      expect(result).toStrictEqual(new Error('Property output must be a string'));
    });
  });
});
