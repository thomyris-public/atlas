import * as fs from 'fs';
import { EOL } from 'os';

import { Logger } from './logger';
import { colours } from './logColours';

describe('Logger', () => {
  const logger = new Logger();
  const spyError = jest.spyOn(console, 'error');
  const spyInfo = jest.spyOn(console, 'info');
  const spyReadFileSync = jest.spyOn(fs, 'readFileSync');

  beforeEach(() => {
    jest.clearAllMocks();
  });

  let time: any; // eslint-disable-line @typescript-eslint/no-explicit-any

  /* eslint @typescript-eslint/no-empty-function: "off" */
  beforeAll(() => {
    // avoid geting the log message in the terminal comment to see log in console
    spyError.mockImplementation(() => {});
    spyInfo.mockImplementation(() => {});

    time = new Date(0);
    const _GLOBAL: any = global;
    _GLOBAL.Date = class {
      public static now() {
        return time;
      }

      constructor() {
        return time;
      }

      public valueOf() {
        return time;
      }
    };
  });

  const name = 'test';
  const version = '0.0.0';
  const fileString = `{"name": "${name}", "version": "${version}"}`;

  it('should call console.error with the given message and stack', () => {
    // arrange
    spyReadFileSync.mockReturnValueOnce(fileString);
    const date = new Date().toLocaleTimeString();
    const heading = `${colours.fg.magenta}${name} version ${version} at ${date}${colours.reset}${EOL}`;
    const message = 'test';
    const stack = 'line 0';
    const errorString = `${heading}${colours.fg.red}Error: ${message}${colours.reset}${EOL}Stack: ${stack}${EOL}`;
    // act
    logger.error(message, stack);
    // assert
    expect(spyError).toHaveBeenNthCalledWith(1, errorString);
  });

  it('should call console.info with the given message', () => {
    // arrange
    spyReadFileSync.mockReturnValueOnce(fileString);
    const date = new Date().toLocaleTimeString();
    const heading = `${colours.fg.magenta}${name} version ${version} at ${date}${colours.reset}${EOL}`;
    const message = 'test';
    const infoString = `${heading}${colours.fg.green}Info: ${message}${colours.reset}${EOL}`;
    // act
    logger.info(message);
    // assert
    expect(spyInfo).toHaveBeenNthCalledWith(1, infoString);
  });

  it('should throw if the file is not a json', () => {
    // arrange
    spyReadFileSync.mockReturnValueOnce('azeaze');
    const message = 'test';
    // act
    let result: any;
    try {
      logger.info(message);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toBeTruthy();
    expect(spyInfo).not.toHaveBeenCalled();
  });
});
