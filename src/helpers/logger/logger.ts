import { EOL } from 'os';
import { join } from 'path';
import { readFileSync } from 'fs';

import { colours } from './logColours';

export interface LoggerInterface {
  error(message: string, stack?: string): void;
  info(message: string): void;
}

export class Logger implements LoggerInterface {
  private baseColour = colours.reset;
  private headingColour = colours.fg.magenta;
  private errorColour = colours.fg.red;
  private infoColour = colours.fg.green;

  private heading = (): string => {
    const infoFile = this.getInfo();
    const date = new Date().toLocaleTimeString();
    return `${this.headingColour}${infoFile.name} version ${infoFile.version} at ${date}${this.baseColour}${EOL}`;
  };

  error(message: string, stack?: string) {
    let errorString = `${this.heading()}${this.errorColour}Error: ${message}${this.baseColour}`;
    if (stack && typeof stack === 'string') errorString += `${EOL}Stack: ${stack}${EOL}`;
    console.error(errorString);
  }

  info(message: string) {
    const infoString = `${this.heading()}${this.infoColour}Info: ${message}${this.baseColour}${EOL}`;
    console.info(infoString);
  }

  private getInfo(): { name: string; version: string } {
    try {
      const path = join(__dirname, '../../assets/info.json');
      const file = readFileSync(path, 'utf-8');
      const info = JSON.parse(file);
      return info;
    } catch (error) {
      throw error;
    }
  }
}
