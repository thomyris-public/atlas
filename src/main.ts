#!/usr/bin/env node

import { Atlas } from './scripts/atlas';
import { Walker } from './scripts/walker/walker';
import { Logger } from './helpers/logger/logger';
import { GetConf } from './helpers/getConf/getConf';
import { GetLayout } from './scripts/getLayout/getLayout';
import { GetHeaders } from './scripts/getHeaders/getHeaders';
import { GetBacklink } from './scripts/getBacklink/getBackLink';
import { ErrorHandler } from './helpers/errorHandler/errorHandler';
import { GetBreadcrumb } from './scripts/getBreadcrumb/getBreadcrumb';
import { FileConverter } from './scripts/fileConverter/fileConverter';
import { CreateHtmlFile } from './scripts/createHtmlFile/createHtmlFile';

// helpers
const logger = new Logger();
const getConf = new GetConf();
const errorHandler = new ErrorHandler(logger);

const fileConverter = new FileConverter();
const getHeaders = new GetHeaders();
const getLayout = new GetLayout();
const getBreadcrumb = new GetBreadcrumb(getConf);
const getBackLink = new GetBacklink();

const createHtmlFile = new CreateHtmlFile(fileConverter, getHeaders, getLayout, getBreadcrumb, getBackLink);
const walker = new Walker(createHtmlFile);

new Atlas(walker, getConf, logger, errorHandler).execute();
