import { EOL } from 'os';

const file = `# Heading level 1

## Heading with custom id{#custom-id}

~~_**The world is**_~~ flat.

_______________________________________________________________

# ordered list with a sub ordered list
1. First item
2. Second item
  1. First sub item
  2. Second sub item
3. Third item
4. Fourth item
- First unord item

# unordered list
- First item
- Second item
- Third item
- Fourth item 

# check list
- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media

\`inline\` code

\`inline _style_\` code

\`\`\`
code block
toto
\`\`\`

asasaasa [link with tooltip](https://www._link_.io "tooltip") azeaaaa ![image with link](https://www.link.image "super phrase de test du tooltip")

[![image with link](https://www.link.image)](https://www.link.io)

![alt test](https://www.link.image){size: '150x800', align: 'center'}

![](https://www.link.image){title: 'je suis un titre', size: '150x800', align: 'right'}

![alt replace title](https://www.link.image){title: 'je suis un titre', size: '150x800', align: 'left'}

![](https://www.link.image){size: '150x800', align: 'left'}

![](https://www.link.image){size: '150x800'}

![](https://www.link.image){title: 'je suis un titre'}

![](https://www.link.image "tooltip replace the title"){title: 'je suis un titre'}

[Heading IDs](#heading-level-1)

| test     | test1 | test2 | test3 |
| :----------- | :-------: |--------: |-------- |
| elm     | elmt1      |elmt2|

| test     | test1 | test2 |
| ----------- | --------: |
| elm      | elmt1       |


First Term
: This is the definition of the first term.

Here's a simple footnote,[^1].

[^1]: This is the first footnote.
Second part of the footnote.

> Block quote
>
>> block quote inside a block quote
>> second depth
>>> third depth
> first depth
`;

// build the converted file
let convertedFile = '';

convertedFile += `  <h1 id = "heading-level-1">Heading level 1</h1>${EOL}${EOL}`;
convertedFile += `  <h2 id = "custom-id">Heading with custom id</h2>${EOL}${EOL}`;
convertedFile += `  <p><s><span style="font-style: italic;"><span style="font-weight: bold;">The world is</span></span></s> flat.</p>${EOL}${EOL}`;
convertedFile += `  <hr>${EOL}${EOL}`;

// list part
convertedFile += `  <h1 id = "ordered-list-with-a-sub-ordered-list">ordered list with a sub ordered list</h1>${EOL}${EOL}`;
convertedFile += `  <ol>${EOL}`;
convertedFile += `    <li>First item</li>${EOL}`;
convertedFile += `    <li>Second item</li>${EOL}`;
convertedFile += `    <ol>${EOL}`;
convertedFile += `      <li>First sub item</li>${EOL}`;
convertedFile += `      <li>Second sub item</li>${EOL}`;
convertedFile += `    </ol>${EOL}`;
convertedFile += `    <li>Third item</li>${EOL}`;
convertedFile += `    <li>Fourth item</li>${EOL}`;
convertedFile += `  </ol>${EOL}${EOL}`;
convertedFile += `  <ul>${EOL}`;
convertedFile += `      <li>First unord item</li>${EOL}`;
convertedFile += `  </ul>${EOL}${EOL}`;

convertedFile += `  <h1 id = "unordered-list">unordered list</h1>${EOL}${EOL}`;
convertedFile += `  <ul>${EOL}`;
convertedFile += `    <li>First item</li>${EOL}`;
convertedFile += `    <li>Second item</li>${EOL}`;
convertedFile += `    <li>Third item</li>${EOL}`;
convertedFile += `    <li>Fourth item </li>${EOL}`;
convertedFile += `  </ul>${EOL}${EOL}`;

// check list
convertedFile += `  <h1 id = "check-list">check list</h1>${EOL}${EOL}`;
convertedFile += `  <ul>${EOL}`;
convertedFile += `    <li class = "checked">Write the press release</li>${EOL}`;
convertedFile += `    <li class = "unChecked">Update the website</li>${EOL}`;
convertedFile += `    <li class = "unChecked">Contact the media</li>${EOL}`;
convertedFile += `  </ul>${EOL}${EOL}`;

// inline code part
convertedFile += `  <p><code>inline</code> code</p>${EOL}${EOL}`;
convertedFile += `  <p><code>inline _style_</code> code</p>${EOL}${EOL}`;

// code block part
convertedFile += `  <pre>${EOL}`;
convertedFile += `code block${EOL}`;
convertedFile += `toto${EOL}`;
convertedFile += `  </pre>${EOL}${EOL}`;

// link and image part
convertedFile += `  <p><div>asasaasa <a href = "https://www._link_.io" data-tooltip  = "tooltip">link with tooltip</a> azeaaaa <figure class="content-media undefined" role="group" aria-label="super phrase de test du tooltip"><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" alt="super phrase de test du tooltip"><div class="content-media__modal-caption">super phrase de test du tooltip</div></div><div class="content-media__img" data-tooltip="super phrase de test du tooltip"><img src="https://www.link.image" alt="super phrase de test du tooltip" ></div><figcaption class="content-media__caption">super phrase de test du tooltip</figcaption></div></figure></div></p>${EOL}${EOL}`;
convertedFile += `  <p><div><a href = "https://www.link.io" ><figure class="content-media undefined" role="group" aria-label="image with link"><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" alt="image with link"><div class="content-media__modal-caption">image with link</div></div><div class="content-media__img" data-tooltip="image with link"><img src="https://www.link.image" alt="image with link" ></div><figcaption class="content-media__caption">image with link</figcaption></div></figure></a></div></p>${EOL}${EOL}`;
convertedFile += `  <p><div><figure class="content-media content-media--center" role="group" aria-label="alt test"><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" alt="alt test"><div class="content-media__modal-caption">alt test</div></div><div class="content-media__img" data-tooltip="alt test"><img src="https://www.link.image" alt="alt test" style="height: 150px; width: 800px;"></div><figcaption class="content-media__caption">alt test</figcaption></div></figure></div></p>${EOL}${EOL}`;
convertedFile += `  <p><div><figure class="content-media content-media--right" role="group" aria-label="je suis un titre"><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" alt="je suis un titre"><div class="content-media__modal-caption">je suis un titre</div></div><div class="content-media__img" data-tooltip="je suis un titre"><img src="https://www.link.image" alt="je suis un titre" style="height: 150px; width: 800px;"></div><figcaption class="content-media__caption">je suis un titre</figcaption></div></figure></div></p>${EOL}${EOL}`;
convertedFile += `  <p><div><figure class="content-media content-media--left" role="group" aria-label="je suis un titre"><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" alt="je suis un titre"><div class="content-media__modal-caption">je suis un titre</div></div><div class="content-media__img" data-tooltip="je suis un titre"><img src="https://www.link.image" alt="je suis un titre" style="height: 150px; width: 800px;"></div><figcaption class="content-media__caption">je suis un titre</figcaption></div></figure></div></p>${EOL}${EOL}`;
convertedFile += `  <p><div><figure class="content-media content-media--left" role="group" ><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" ></div><div class="content-media__img" ><img src="https://www.link.image"  style="height: 150px; width: 800px;"></div></div></figure></div></p>${EOL}${EOL}`;
convertedFile += `  <p><div><figure class="content-media content-media--left" role="group" ><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" ></div><div class="content-media__img" ><img src="https://www.link.image"  style="height: 150px; width: 800px;"></div></div></figure></div></p>${EOL}${EOL}`;
convertedFile += `  <p><div><figure class="content-media content-media--left" role="group" aria-label="je suis un titre"><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" alt="je suis un titre"><div class="content-media__modal-caption">je suis un titre</div></div><div class="content-media__img" data-tooltip="je suis un titre"><img src="https://www.link.image" alt="je suis un titre" ></div><figcaption class="content-media__caption">je suis un titre</figcaption></div></figure></div></p>${EOL}${EOL}`;
convertedFile += `  <p><div><figure class="content-media content-media--left" role="group" aria-label="tooltip replace the title"><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="https://www.link.image" alt="tooltip replace the title"><div class="content-media__modal-caption">tooltip replace the title</div></div><div class="content-media__img" data-tooltip="tooltip replace the title"><img src="https://www.link.image" alt="tooltip replace the title" ></div><figcaption class="content-media__caption">tooltip replace the title</figcaption></div></figure></div></p>${EOL}${EOL}`;
convertedFile += `  <p><a href = "#heading-level-1" >Heading IDs</a></p>${EOL}${EOL}`;

// table part
convertedFile += `  <table>${EOL}`;
convertedFile += `    <tr>${EOL}`;
convertedFile += `      <th class = "colLeft">test</th>${EOL}`;
convertedFile += `      <th class = "colCenter">test1</th>${EOL}`;
convertedFile += `      <th class = "colRight">test2</th>${EOL}`;
convertedFile += `      <th>test3</th>${EOL}`;
convertedFile += `    </tr>${EOL}`;
convertedFile += `    <tr>${EOL}`;
convertedFile += `      <td class = "colLeft">elm</td>${EOL}`;
convertedFile += `      <td class = "colCenter">elmt1</td>${EOL}`;
convertedFile += `      <td class = "colRight">elmt2</td>${EOL}`;
convertedFile += `      <td></td>${EOL}`;
convertedFile += `    </tr>${EOL}`;
convertedFile += `  </table>${EOL}${EOL}`;

// bad written md table
convertedFile += `  <p>|test|test1|test2|<br>${EOL}`;
convertedFile += `|-----------|--------:|<br>${EOL}`;
convertedFile += `|elm|elmt1|${EOL}`;
convertedFile += `</p>${EOL}${EOL}`;

// definition list
convertedFile += `  <dl>${EOL}`;
convertedFile += `    <dt>First Term</dt>${EOL}`;
convertedFile += `    <dd>This is the definition of the first term.</dd>${EOL}`;
convertedFile += `  </dl>${EOL}${EOL}`;

// footnote link
convertedFile += `  <p>Here's a simple footnote,<sup id = "1"><a href = "#1fn">1</a></sup>.</p>${EOL}${EOL}`;

// block quote
convertedFile += `  <blockquote>${EOL}`;
convertedFile += `    <p>Block quote</p>${EOL}`;
convertedFile += `    <p></p>${EOL}`;
convertedFile += `    <blockquote>${EOL}`;
convertedFile += `      <p>block quote inside a block quote</p>${EOL}`;
convertedFile += `      <p>second depth</p>${EOL}`;
convertedFile += `      <blockquote>${EOL}`;
convertedFile += `        <p>third depth</p>${EOL}`;
convertedFile += `      </blockquote>${EOL}`;
convertedFile += `    </blockquote>${EOL}`;
convertedFile += `    <p>first depth</p>${EOL}`;
convertedFile += `  </blockquote>${EOL}${EOL}`;

// footnote always at the end of the file
convertedFile += `<hr>${EOL}`;
convertedFile += `  <p id = "1fn">${EOL}`;
convertedFile += `1. This is the first footnote.<br>${EOL}`;
convertedFile += `Second part of the footnote. <a href = "#1">&#x21A9;&#xFE0E;</a>${EOL}`;
convertedFile += `  </p>${EOL}`;

const fileEmpty = '\r\n\n\n';
const convertedFileEmpty = '';

export { file, convertedFile, fileEmpty, convertedFileEmpty };
