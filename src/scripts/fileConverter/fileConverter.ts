import { EOL } from 'os';

export interface FileConverterInterface {
  convertMdToHtml(mdString: string): string;
}

/**
 * Represents attributes for image.
 */
type ImageAttribute = {
  title?: string;
  size?: {
    height: number;
    width: number;
  };
  align?: string;
};

export class FileConverter implements FileConverterInterface {
  // store all the lines
  private splitedFile: string[] = [];

  // loop iteration number
  private loopIter = 0;

  // used to know if the line is part of a html block
  private isHtmlBlock = false;

  // used to know if we are in a markdown block
  private isMdBlock = false;

  // store the lines present in a block, have a setter for testing issue
  private savedLines: string[] = [];

  // store the footNotes
  private footNotes: Array<string[]> = [];

  // store the attributes images
  private attributesImage: ImageAttribute = {};

  // generate an indentation
  private indentation(nbr: number) {
    if (nbr < 0) nbr = 0;
    return '  '.repeat(nbr + 1);
  }

  private isInInlineBlock(isIn: RegExpMatchArray | null, isStyled: RegExpMatchArray) {
    if (isIn) {
      if (isIn.index! < isStyled.index! && isIn.index! + isIn[0].length > isStyled.index!) return true;
    }
    return false;
  }

  // trim only the begining of a string
  private ltrim(string: string) {
    return string.replace(/^\s+/g, '');
  }

  convertMdToHtml(mdString: string): string {
    const result: string[] = [];

    // split the string line by line
    this.splitedFile = mdString.split(/(?<=\n)/).map((elmt) => elmt.replace(/(\n)|(\r\n)/g, ''));

    // loop on each line and convert it
    for (; this.loopIter < this.splitedFile.length; this.loopIter++) {
      const line: string = this.splitedFile[this.loopIter];
      result.push(this.convertLine(line));
    }

    // add all the footNotes at the end of the file string
    for (let i = 0; i < this.footNotes.length; i++) {
      this.isHtmlBlock = true;
      const footNote = this.footNotes[i];
      const regex = new RegExp(/^\[\^.*\]: /);

      // add a separator if there is at least one footnote
      if (i === 0) result.push(`<hr>${EOL}`);
      let fnId = '';

      let fnString = '';

      // loop on each footnote's line
      for (let j = 0; j < footNote.length; j++) {
        const fnLine: string = footNote[j];
        const match = fnLine.match(regex);

        // if the line is the footnote's start then open a block
        if (match) {
          let fnContent: string = fnLine.slice(match.index! + match[0].length);
          fnContent = this.convertMdLine(fnContent).trim();
          fnId = match[0].replace(/\[|\^|]|:/g, '').trim();
          fnString += `${this.indentation(0)}<p id = "${fnId}fn">${EOL}${fnId}. ${fnContent}`;
        } else {
          fnString += `<br>${EOL}${fnLine}`;
        }
      }

      // add the backlink and close the block
      const backLink = `<a href = "#${fnId}">&#x21A9;&#xFE0E;</a>`;
      fnString += ` ${backLink}${EOL}${this.indentation(0)}</p>${EOL}`;
      result.push(fnString);
    }

    // reset the footnotes
    this.footNotes = [];
    // return the converted string

    const convertedFile = result.join('');

    this.loopIter = 0;
    return convertedFile;
  }

  private convertLine(line: string): string {
    line = this.convertBlockCode(line);
    line = this.convertList(line);
    line = this.convertTable(line);
    line = this.convertDefinitionList(line);
    line = this.saveFootNote(line);
    line = this.convertBlockQuote(line);
    line = this.convertMdLine(line);

    // reset the property
    this.isHtmlBlock = false;

    if (this.isMdBlock || line === '') return line;
    return `${this.indentation(0)}${line}${EOL}${EOL}`;
  }

  private convertMdLine(line: string): string {
    if (!this.isMdBlock) {
      line = this.convertStraightLine(line);
      line = this.convertInlineCode(line);
      line = this.convertFontStyle(line);
      line = this.convertImage(line);
      line = this.convertLink(line);
      line = this.convertTitle(line);
      line = this.createFootNoteRef(line);
    }

    // assign true if isConverted or isHtmlBlock are true
    if (this.isHtmlBlock === false) line = this.convertParagraphe(line);
    return line;
  }

  private convertStraightLine(line: string) {
    const regex = /^(-|_|\*){3,}$/;
    if (regex.test(line)) {
      line = '<hr>';
      this.isHtmlBlock = true;
    }
    return line;
  }

  private convertFontStyle(line: string): string {
    const fontStyleChecker: { md: string[]; htmlElement: string }[] = [
      {
        md: ['\\*\\*', '__'],
        htmlElement: '<span style="font-weight: bold;"></span>',
      },
      {
        md: ['\\*', '_'],
        htmlElement: '<span style="font-style: italic;"></span>',
      },
      {
        md: ['~~'],
        htmlElement: '<s></s>',
      },
      {
        md: ['=='],
        htmlElement: '<mark></mark>',
      },
    ];

    // loop on specified checker
    for (let i = 0; i < fontStyleChecker.length; i++) {
      // bold or italic
      const checker = fontStyleChecker[i];

      // loop on markdown delimiter
      for (let i = 0; i < checker.md.length; i++) {
        const mdDelimiter = checker.md[i];
        const mdStyle = `${mdDelimiter}(.*?)${mdDelimiter}`;

        const regexStyle = new RegExp(mdStyle);
        const regexLinkRef = new RegExp(`\\(.[^()]*?${mdStyle}.*?\\)`);
        const regexCodeInline = new RegExp(`<code>.*${mdStyle}.*</code>`);

        // check if the line contains the delimiter
        const isStyle = line.match(regexStyle);

        // check if the line contains a link ref with the delimiter
        const isInLinkRef = line.match(regexLinkRef);

        // check if the line contains a code inline with the delimiter
        const isInCodeInline = line.match(regexCodeInline);

        // if the line is a style and not in a link ref or a code inline
        const isStyleAndNotInLinkRefOrCodeInline = isStyle && !this.isInInlineBlock(isInLinkRef, isStyle) && !this.isInInlineBlock(isInCodeInline, isStyle);

        if (isStyleAndNotInLinkRefOrCodeInline) {
          const content = isStyle[1];
          const subStyleInContent = this.convertFontStyle(content);

          // inject the content inside the element
          const styleHtml = checker.htmlElement.replace(/></, '>md@md<').split(/md@md/); // replace the first match by >md@md< to split properly
          styleHtml.splice(1, 0, subStyleInContent);
          const styleHtmlString = styleHtml.join('');

          const newLine = line.replace(`${mdDelimiter.replace(/\\/g, '')}${content}${mdDelimiter.replace(/\\/g, '')}`, styleHtmlString);
          const convertLine = this.convertFontStyle(newLine);
          return convertLine;
        }
      }
    }

    return line;
  }

  private convertImage(line: string) {
    const regexImage = new RegExp(/!\[.*?(\]\().*?\)\{.*?\}|!\[.*?(\]\().*?\)/);
    const haveImage = line.match(regexImage);
    if (haveImage && haveImage.index !== undefined) {
      // cut the string image
      const imageString = haveImage[0];
      const startOfline = line.slice(0, haveImage.index);
      const endofLine = line.slice(haveImage.index + imageString.length, line.length);

      // get the image's alt
      const match1 = imageString.match(/\[.*\]/);
      const imageContent = match1![0].replace(/^\[|\]$/g, '');

      // get the image ref
      const match2 = imageString.match(/\(.*\)/);
      const imageConf = match2![0].replace(/\(|\)/g, '');
      const imageRef = imageConf.split(/ /)[0];

      // get the image tooltip
      const match3 = imageConf.match(/".*"/);
      const imageTooltip = match3 ? match3[0].replace(/"/g, '') : '';

      // generate the html link
      let htmlImage = '';
      let captionAlt;
      let styleAttribute = '';
      let dataTooltip;
      let classes;

      // get the image attributes
      const attributesMatched: any = new RegExp(/\{.*\}/, 'i').exec(imageString);
      if (attributesMatched !== null) {
        // processing of attributes before rewriting them
        let attributes: any = attributesMatched[0]
          .toString('utf8')
          .replace(/{\s|\s}|{|}/gi, '')
          .replace(/'|"/g, '')
          .split(',');

        /* istanbul ignore if */
        if (attributes.includes('')) return line;
        else attributes = attributes.map((value: string) => value.trim());

        // get image parameters
        const { css, htmlClass } = this.getImageParameters(attributes);
        /* istanbul ignore next */
        classes = htmlClass ? htmlClass : '';
        /* istanbul ignore next */
        styleAttribute = css ? `style="${css}"` : '';
      }

      if (imageTooltip) {
        dataTooltip = imageTooltip;

        if (!this.attributesImage.title || !imageContent) {
          captionAlt = imageTooltip;
        }
      } else if (this.attributesImage.title) {
        dataTooltip = this.attributesImage.title;
        captionAlt = this.attributesImage.title;
      } else if (imageContent) {
        dataTooltip = imageContent;
        captionAlt = imageContent;
      }

      // write image dom element
      htmlImage = `${startOfline}<figure class="content-media ${classes}" role="group" ${
        captionAlt ? `aria-label="${captionAlt}"` : ''
      }><div class="content-media__body"><div class="content-media__modal"><img class="content-media__modal-img" src="${imageRef}" ${captionAlt ? `alt="${captionAlt}"` : ''}>${
        captionAlt ? `<div class="content-media__modal-caption">${captionAlt}</div>` : ''
      }</div><div class="content-media__img" ${dataTooltip ? `data-tooltip="${dataTooltip}"` : ''}><img src="${imageRef}" ${
        captionAlt ? `alt="${captionAlt}"` : ''
      } ${styleAttribute}></div>${captionAlt ? `<figcaption class="content-media__caption">${captionAlt}</figcaption>` : ''}</div></figure>${endofLine}`;

      // wrap the image in a div only if it is not already in a block ex: a table
      if (this.isHtmlBlock === false) {
        htmlImage = `<div>${htmlImage}</div>`;
      }

      // convert the next image in the line
      htmlImage = this.convertImage(htmlImage);
      this.isHtmlBlock = false;
      return htmlImage;
    }

    return line;
  }

  /**
   * Function to re-transform markdown attributes.
   *
   * @param attributes Represents a list of attributes
   */
  private sanitizeAttributes(attributes: string[]): string {
    const sanitizedAttributes = attributes.map((attribute: string, idx: number) => {
      const attributeSplitted: string[] = attribute.split(':');
      let attributesParsed: any;

      if (attributeSplitted.length > 1) {
        attributesParsed = attributeSplitted.map((value: string) => {
          const sizes = value.split('x');

          if (sizes.length > 1) {
            const height = Number(sizes[0]);
            const width = Number(sizes[1]);

            return `{ "height": ${height}, "width": ${width} }`;
          } else {
            return `"${value.trim()}"`;
          }
        });
      }

      return `${idx === 0 ? '{ ' : ''}${attributesParsed[0]}: ${attributesParsed[1]}${idx === attributes.length - 1 ? ' }' : ''}`;
    });

    return sanitizedAttributes.join();
  }

  /**
   * Function to get the image parameters.
   *
   * @param attributes Represents a list of attributes
   */
  private getImageParameters(attributes: string[]): Record<string, string | undefined> {
    let css: string | undefined;
    let htmlClass = 'content-media--left';
    const sanitizedAttributes = this.sanitizeAttributes(attributes);

    /* istanbul ignore next */
    try {
      this.attributesImage = JSON.parse(sanitizedAttributes);
    } catch (error) {
      throw new Error('While processing the image an error occurred.\nPlease check your markdown file.');
    }

    if (this.attributesImage.size !== undefined) {
      css = `height: ${this.attributesImage.size.height}px; width: ${this.attributesImage.size.width}px;`;
    }

    if (this.attributesImage.align !== undefined) {
      switch (this.attributesImage.align.toLowerCase()) {
        case 'center':
          htmlClass = 'content-media--center';
          break;
        case 'right':
          htmlClass = 'content-media--right';
          break;
        case 'left':
        default:
          htmlClass = 'content-media--left';
          break;
      }
    }

    return { css, htmlClass };
  }

  private convertLink(line: string) {
    const regexLink = new RegExp(/\[.*?(\]\().*?\)/);
    const haveLink = line.match(regexLink);

    if (haveLink && haveLink.index !== undefined) {
      // check if is an image and return the line if it is

      // cut the string between the link and the rest
      const link = haveLink[0];
      const startOfline = line.slice(0, haveLink.index);
      const endofLine = line.slice(haveLink.index + link.length, line.length);

      // get all the link info

      // get the link's content
      const match1 = link.match(/\[.*\]/);
      const linkContent = match1![0].replace(/^\[|\]$/g, '');

      // get the link's href
      const match2 = link.match(/\([^(|)]*\)$/);
      const linkConf = match2![0].replace(/\(|\)/g, '');
      const linkRef = linkConf.split(/ "/)[0].replace(/.md$/, '.html');

      // get the link's tooltip
      const match3 = linkConf.match(/".*"/);
      const linkTooltip = match3 ? match3[0] : '';

      // use html data attribute data-tooltip to display tooltip
      const title = linkTooltip ? `data-tooltip  = ${linkTooltip}` : '';

      // generate the html link
      let htmlLink = `${startOfline}<a href = "${linkRef}" ${title}>${linkContent}</a>${endofLine}`;

      // convert the next link in the line
      htmlLink = this.convertLink(htmlLink);
      return htmlLink;
    }
    return line;
  }

  private convertTitle(line: string) {
    if (/(^(#){1,6} )/.test(line)) {
      const match = line.split(/ /)[0].match(/#/g);
      const titleLevel = match!.length;
      const displayedContent = line.replace(/<((?!<).)*>/g, '');

      // default id
      let id = displayedContent;

      const customId = line.match(/{#.*}/);

      if (customId) {
        id = customId[0].replace(/{|}/g, '');
        line = line.substring(0, customId.index) + line.substring(customId.index! + customId[0].length);
      }

      const titleId = id
        .replace(/\#|\[|\]|\(|\)|\:|\.|\\|\`|\//g, '')
        .trim()
        .replace(/{|}|@| |ç|à|è|é|ù|â|ê|î|ô|û|ä|ë|ï|ö|ü|Ç|À|Â|Ä|É|È|Ê|Ë|Î|Ï|Ô|Ö|Ù|Û|Ü|Ÿ|'|"/g, '-')
        .replace(/\s|:/g, '-')
        .toLowerCase();

      const htmlContent = line.replace(/^(#| )*/g, '');

      line = `<h${titleLevel} id = "${titleId}">${htmlContent}</h${titleLevel}>`;
      this.isHtmlBlock = true;
    }
    return line;
  }

  private convertParagraphe(line: string): string {
    if (line) return `<p>${line}</p>`;
    return line;
  }

  private convertInlineCode(line: string): string {
    // used for the regex and to replace the markdown tag from the string
    const identifiers = ['```', '`'];

    // loop on each identifiers
    for (let i = 0; i < identifiers.length; i++) {
      const identifier = identifiers[i];
      const mdIdentifier = `${identifier}((?!${identifier}).)*${identifier}`;
      const regexInlineCode = new RegExp(mdIdentifier);

      const regexLinkRef = new RegExp(`\\(.[^()]*?${mdIdentifier}.*?\\)`);
      const regexCodeInline = new RegExp(`<code>.*${mdIdentifier}.*</code>`);

      // check if the line contains the delimiter
      const isInlineCode = line.match(regexInlineCode);

      // check if inline code is inside a link or image markdown identifier
      const isInLinkRef = line.match(regexLinkRef);

      // check if the line contains a code inline with the delimiter
      const isInCodeInline = line.match(regexCodeInline);

      // if the line is a style and not in a link ref or a code inline
      const haveInlineCode = isInlineCode && !this.isInInlineBlock(isInLinkRef, isInlineCode) && !this.isInInlineBlock(isInCodeInline, isInlineCode);

      if (haveInlineCode) {
        const inlineCode = isInlineCode[0];
        const startOfline = line.slice(0, isInlineCode.index);
        let endofLine = line.slice(isInlineCode.index! + inlineCode.length, line.length);

        // add the HTML tag
        const htmlInlineCode = `<code>${inlineCode}</code>`;

        // convert the rest of the line if needed
        endofLine = this.convertInlineCode(endofLine);

        // remove the markdown tag and build the full line
        const regexReplace = new RegExp(`${identifier}`, 'g');
        const htmlLine = `${startOfline}${htmlInlineCode.replace(regexReplace, '')}${endofLine}`;

        return htmlLine;
      }
    }

    return line;
  }

  private convertBlockCode(line: string): string {
    // used for the regex and to replace the markdown tag from the string
    const identifier = '```';
    const regex = new RegExp(`^${identifier}$`);
    const isBlockTag = regex.test(line);

    const isInBlock = regex.test(this.savedLines[0]);

    if (isBlockTag || isInBlock) {
      this.isMdBlock = true;
      this.savedLines.push(line);

      // write the html block
      if (isBlockTag && isInBlock) {
        const htmlBlockCode = this.savedLines
          .join(`${EOL}`)
          .replace(/```/, '<pre>')
          .replace(/```/, `${this.indentation(0)}</pre>`);
        this.isMdBlock = false;
        this.isHtmlBlock = true;
        this.savedLines = [];

        return htmlBlockCode;
      }

      return '';
    }

    return line;
  }

  private convertList(line: string): string {
    const listIdentifiers: { md: string[]; htmlElement: { startTag: string; endTag: string } }[] = [
      {
        md: ['[0-9]+.'],
        htmlElement: {
          startTag: '<ol>',
          endTag: '</ol>',
        },
      },
      {
        md: ['-', '\\*', '\\+'],
        htmlElement: {
          startTag: '<ul>',
          endTag: '</ul>',
        },
      },
    ];

    for (let i = 0; i < listIdentifiers.length; i++) {
      // used for the regex and to replace the markdown tag from the string
      const identifier = listIdentifiers[i];
      const orRegexIdentifier = `${listIdentifiers[0].md[0]}|${listIdentifiers[1].md[0]}|${listIdentifiers[1].md[1]}|${listIdentifiers[1].md[2]}`;

      for (let i = 0; i < identifier.md.length; i++) {
        const mdTag = identifier.md[i];
        const regex = new RegExp(`^(  )*${mdTag} .*$`);

        // check if line is a list item
        const isListItem = regex.test(line);

        if (isListItem) {
          // detect that we are parsing a "block" and save the line
          this.isMdBlock = true;
          this.savedLines.push(line);

          // check if the next line is also inside the list
          const regexNextLine = new RegExp(`^(  )*(${orRegexIdentifier}) .*$`);
          const isStillInList: boolean = regexNextLine.test(this.splitedFile[this.loopIter + 1]);

          // if current line is in list and not the next then we create the html list and return it
          if (isListItem && !isStillInList) {
            let indentationNbr = 0; // used to indent html lines
            let htmlBlockCode = '';
            this.isHtmlBlock = true;

            // save opened list used to close list blocks
            let openedBlocks: { tag: string; indentation: number }[] = [];

            for (let i = 0; i < this.savedLines.length; i++) {
              let currentLine: string = this.savedLines[i];
              const previousLine: string = this.savedLines[i - 1];

              // get the current line's indentation to know if it is a new list inside the current list
              const lineContent: string = currentLine.slice(0, currentLine.indexOf(`${mdTag.replace(/\\/, '')} `));
              const matchIndentation = lineContent.match(/  /g);
              const indentation: number = matchIndentation ? matchIndentation.length : 0;

              let previousMatchIndentation: any[] | null = null;
              let previousIndentation = -1;

              currentLine = this.ltrim(currentLine);
              // get the previous line's indentation to know if we are still in the subList or not
              if (previousLine) {
                const previousLineContent = previousLine.slice(0, previousLine.indexOf(`${mdTag} `));
                previousMatchIndentation = previousLineContent.match(/  /g);
                previousIndentation = previousMatchIndentation ? previousMatchIndentation.length : 0;
              }

              // if the current line's indentation > previous line then we create a new list block
              if (indentation > previousIndentation) {
                // find the current identifier to find wich list block we need to open
                let currentIdentifier = listIdentifiers.find((elmt) => {
                  return elmt.md.find((elmt) => {
                    const regex = new RegExp(`^(  )*${elmt} .*$`);
                    const isIdentifier: boolean = regex.test(currentLine);
                    if (isIdentifier) return elmt;
                  });
                });

                // if identifier not found give ul as default => should not happen
                /* istanbul ignore next */
                if (!currentIdentifier) currentIdentifier = listIdentifiers[1];

                const currentHtml = currentIdentifier.htmlElement;

                // do not indent the first line
                if (i === 0) currentLine = `${currentHtml.startTag}${EOL}${currentLine}`;
                else currentLine = `${this.indentation(indentationNbr)}${currentHtml.startTag}${EOL}${currentLine}`;

                // add 1 indentation because just opened a html block
                indentationNbr++;
                openedBlocks.push({ tag: currentHtml.endTag, indentation: indentation });
              }

              // define the item classes
              let listItemCLass = '';

              // check if the list item is a check box
              const isCheckBox: boolean = new RegExp(`(${orRegexIdentifier}) (\\[ ]|\\[x])`).test(currentLine);

              // add the proper check box class
              if (isCheckBox) {
                const unCheckedRegex = new RegExp(' (\\[ ])');
                const checkedRegex = new RegExp(' (\\[x])');

                if (unCheckedRegex.test(currentLine)) {
                  listItemCLass = ' class = "unChecked"';
                  currentLine = currentLine.replace(unCheckedRegex, '');
                } else if (checkedRegex.test(currentLine)) {
                  listItemCLass = ' class = "checked"';
                  currentLine = currentLine.replace(checkedRegex, '');
                }
              }

              currentLine = currentLine.replace(new RegExp(`(${orRegexIdentifier}) `), `${this.indentation(indentationNbr)}<li${listItemCLass}>`);
              currentLine += `</li>${EOL}`;

              // convert the list item content
              currentLine = this.convertMdLine(currentLine);

              for (let j = 0; j < openedBlocks.length; j++) {
                const openedBlock = openedBlocks[j];

                // get line's the markdown tag
                const mdTag = this.savedLines[i].trim().split(' ')[0];

                // get the identifier for the tag found in the line
                const identifier = listIdentifiers.find((elmt) => {
                  const foundMd = elmt.md.find((elmt) => {
                    const testTag = new RegExp(`^${elmt}$`).test(mdTag);
                    if (testTag) return elmt;
                  });
                  if (foundMd) return elmt;
                });

                // if the current indentation < openedBlock indentation then close the block
                if (indentation < openedBlock.indentation) {
                  // remove 1 indentation because just closed a html block
                  indentationNbr--;

                  const closeBlock = `${this.indentation(indentationNbr)}${openedBlock.tag}${EOL}`;
                  currentLine = `${closeBlock}${this.indentation(indentationNbr)}${this.ltrim(currentLine)}`;
                  openedBlocks.splice(j, 1);
                  j--;
                }

                //  if the current indentation === openedBlock indentation and the identifier are not the same then close the block
                if (indentation === openedBlock.indentation && identifier && openedBlock.tag !== identifier.htmlElement.endTag) {
                  indentationNbr--;

                  currentLine = `${this.indentation(indentationNbr)}${openedBlock.tag}${EOL}${EOL}${this.indentation(indentationNbr)}${
                    identifier.htmlElement.startTag
                  }${EOL}${this.indentation(indentationNbr)}${currentLine}`;

                  openedBlocks.push({ tag: identifier.htmlElement.endTag, indentation });
                  openedBlocks.splice(j, 1);
                  j--;
                }
              }
              htmlBlockCode += currentLine;
            }

            // close all remaining opened blocks there's no lines left
            openedBlocks = openedBlocks.reverse();
            for (let i = 0; i < openedBlocks.length; i++) {
              const closingTag = openedBlocks[i];

              // add breakline only if more list item are present
              let eol = EOL;
              if (i === openedBlocks.length - 1) eol = '';

              indentationNbr--;
              htmlBlockCode += `${this.indentation(indentationNbr)}${closingTag.tag}${eol}`;
            }

            // end of the markdown list block and reset the line saved for a block
            this.isMdBlock = false;
            this.savedLines = [];
            return htmlBlockCode;
          }

          return '';
        }
      }
    }

    // if is not a list item return the line
    return line;
  }

  private convertTable(line: string): string {
    const nextLine: string = this.splitedFile[this.loopIter + 1];

    // check if the line is part of a markdown table
    const regex = new RegExp(/^(\|.*\|)+$/);
    if (regex.test(line)) {
      this.isMdBlock = true;
      this.savedLines.push(line);

      // if the next line is not in the table then we convert the saved line into the html table
      if (!regex.test(nextLine)) {
        // create the table rows
        const rows: Array<Array<string>> = [];
        for (let i = 0; i < this.savedLines.length; i++) {
          const currentLine: string = this.savedLines[i];
          const row: string[] = currentLine.split('|');
          row.splice(0, 1);
          row.splice(-1, 1);
          rows.push(row.map((elmt) => elmt.trim()));
        }

        const headers: string[] = rows[0];
        const tableSeparator: string[] = rows[1];
        const tableContent: Array<Array<string>> = [...rows].splice(2);

        // need atleast as much separators as headers
        // need separators to be properly written
        // return the markdown strings
        if (tableSeparator.length < headers.length || tableSeparator.some((elmt) => !/^:?-+:?$/.test(elmt))) {
          const joinedRows = rows.map((row) => `|${row.join('|')}|`).join(`<br>${EOL}`) + EOL;

          // reset
          this.isMdBlock = false;
          this.savedLines = [];
          return joinedRows;
        }

        const colStyle: string[] = [];

        let headerString = `${this.indentation(1)}<tr>${EOL}`;

        for (let i = 0; i < headers.length; i++) {
          this.isHtmlBlock = true;
          const header: string = this.convertMdLine(headers[i]);
          const separator: string = tableSeparator[i];

          // save the alignment given by the markdown
          if (/^:-+$/.test(separator)) colStyle.push(' class = "colLeft"');
          else if (/^-+:$/.test(separator)) colStyle.push(' class = "colRight"');
          else if (/^:-+:$/.test(separator)) colStyle.push(' class = "colCenter"');
          else colStyle.push('');

          headerString += `${this.indentation(2)}<th${colStyle[i]}>${header}</th>${EOL}`;
        }

        let tableContentString = '';
        // loop on all row exept headers and separators
        for (let i = 0; i < tableContent.length; i++) {
          const row = tableContent[i];
          let rowString = `${this.indentation(1)}<tr>${EOL}`;

          for (let j = 0; j < headers.length; j++) {
            // convert the line, need to reset the mdBlock property
            this.isMdBlock = false;
            const rowElmt = row[j] ? this.convertMdLine(row[j]) : '';
            this.isMdBlock = true;

            rowString += `${this.indentation(2)}<td${colStyle[j]}>${rowElmt}</td>${EOL}`;
          }

          rowString += `${this.indentation(1)}</tr>${EOL}`;
          tableContentString += rowString;
        }

        headerString += `${this.indentation(1)}</tr>${EOL}`;

        const tableString = `<table>${EOL}${headerString}${tableContentString}${this.indentation(0)}</table>`;

        // end of the markdown list block and reset the line saved for a block
        this.isMdBlock = false;
        this.savedLines = [];

        return tableString;
      }
      return '';
    }

    return line;
  }

  private convertDefinitionList(line: string): string {
    const nextLine = this.splitedFile[this.loopIter + 1];
    const regex = new RegExp(/^: .*/);

    // if the next line is in a definition list then save it
    if (regex.test(nextLine)) {
      this.isMdBlock = true;
      this.savedLines.push(line);
      return '';
    }

    // if current line in a definition list and not the next line
    // then create the html block with the saved lines
    if (regex.test(line) && !regex.test(nextLine)) {
      this.isHtmlBlock = true;
      this.savedLines.push(line);

      // create the html block
      let htmlBlock = `<dl>${EOL}`;
      const indentNbr = 1;
      for (let i = 0; i < this.savedLines.length; i++) {
        const savedLine: string = this.savedLines[i].replace(/^: /, '');

        // first saved line is the title
        // the other are the definitions
        if (i === 0) {
          htmlBlock += `${this.indentation(indentNbr)}<dt>${savedLine}</dt>${EOL}`;
        } else {
          htmlBlock += `${this.indentation(indentNbr)}<dd>${savedLine}</dd>${EOL}`;
        }
      }

      htmlBlock += `${this.indentation(0)}</dl>`;
      // reset the saved lines and the mdBlock property
      this.isMdBlock = false;
      this.savedLines = [];
      return htmlBlock;
    }

    return line;
  }

  private saveFootNote(line: string): string {
    const regex = new RegExp(/^\[\^.*\]: /);
    const match = line.match(regex);

    const previousLine: string = this.splitedFile[this.loopIter - 1];
    const latestFootNote = this.footNotes[this.footNotes.length - 1];

    // if line is a footnote save it
    if (match) {
      this.footNotes.push([line]);
      return '';
    }

    // if the line is IN a footnote save it
    if (line !== '' && latestFootNote && previousLine === latestFootNote[latestFootNote.length - 1]) {
      this.footNotes[this.footNotes.length - 1].push(line);
      return '';
    }

    return line;
  }

  private createFootNoteRef(line: string): string {
    const regex = new RegExp(/\[\^[^\]|:]*]/g);
    const matches = line.match(regex);

    // line have at least one footnot ref
    if (matches) {
      // loop on all footnot ref and convert it
      for (let i = 0; i < matches.length; i++) {
        const match = matches[i];
        const id = match.replace(/\[|\^|]|:/g, '').trim();
        const htmlFnRef = `<sup id = "${id}"><a href = "#${id}fn">${id}</a></sup>`;
        line = line.replace(match, htmlFnRef);
      }
    }

    return line;
  }

  private convertBlockQuote(line: string): string {
    const regex = /^>+.*$/;

    const nextLine = this.splitedFile[this.loopIter + 1];

    // save the block quote lines
    if (regex.test(line)) {
      this.isHtmlBlock = true;
      this.isMdBlock = true;
      this.savedLines.push(line);

      // if the next line is not a block quote then convert all the saved lines
      if (!regex.test(nextLine)) {
        let htmlString = `<blockquote>${EOL}`;

        let openedBlocks: number[] = [];
        for (let i = 0; i < this.savedLines.length; i++) {
          const savedLine = this.savedLines[i];
          const previousLine = this.savedLines[i - 1] ? this.savedLines[i - 1] : null;
          const nextLine = this.savedLines[i + 1] ? this.savedLines[i + 1] : null;

          // the number of ">" for the current line
          const depth = savedLine.match(/>/g)!.length;

          // the number of ">" for the previous line default is same as current line
          const previousDepth = previousLine ? previousLine.match(/>/g)!.length : depth;

          // the number of ">" for the next line default is less than current
          const nextDepth = nextLine ? nextLine.match(/>/g)!.length : depth - 1;

          // open a sub blockquote if more ">" than previous line
          if (depth > previousDepth) {
            htmlString += `${this.indentation(depth - 1)}<blockquote>${EOL}`;
            openedBlocks.push(depth);
          }

          htmlString += `${this.indentation(depth)}<p>${savedLine.replace(/(> |>)/g, '')}</p>${EOL}`;

          // close all sub blockquote if more ">" than next line
          if (depth > nextDepth) {
            for (let i = 0; i < openedBlocks.length; i++) {
              const openedBlock = [...openedBlocks].reverse()[i];
              htmlString += `${this.indentation(openedBlock - 1)}</blockquote>${EOL}`;
            }
            openedBlocks = [];
          }
        }

        htmlString += `${this.indentation(0)}</blockquote>`;

        this.isMdBlock = false;
        this.savedLines = [];
        return htmlString;
      }

      return '';
    }

    return line;
  }
}
