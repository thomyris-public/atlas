import { FileConverter } from './fileConverter';

import { convertedFile, convertedFileEmpty, file, fileEmpty } from './fileConverter.test.data';

describe('FileConverter', () => {
  const fileConverter = new FileConverter();

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  describe('#convertMdToHtml', () => {
    it('Should convertLine', () => {
      // arrange
      // act
      const result = fileConverter.convertMdToHtml(file);
      // assert
      expect(result).toStrictEqual(convertedFile);
    });

    it('Should return an empty file', () => {
      // arrange
      // act
      const result = fileConverter.convertMdToHtml(fileEmpty);
      // assert
      expect(result).toStrictEqual(convertedFileEmpty);
    });
  });
});
