import { EOL } from 'os';
import { GetHeaders } from './getHeaders';

describe('GetHeaders', () => {
  const getHeaders = new GetHeaders();

  beforeEach(() => {
    jest.clearAllMocks();
  });

  const headers: string[] = ['<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>', '<title>test</title>', '<link href="test.css" rel="stylesheet">'];

  describe('#get', () => {
    it('Should return the head string with a default title', async () => {
      // arrange
      let expectedString = `<head>${EOL}`;
      expectedString += `<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>${EOL}`;
      expectedString += `<title>test</title>${EOL}`;
      expectedString += `<link href="test.css" rel="stylesheet">${EOL}`;
      expectedString += `</head>${EOL}`;

      // act
      const headString: string = await getHeaders.getHtmlHead('', headers);
      // assert
      expect(headString).toStrictEqual(expectedString);
    });

    it('Should return the head string with a title passed in the headersString', async () => {
      // arrange
      const headersString = `<!-- HeaderBegin -->${EOL}<!-- title${EOL}TITLE${EOL}-->${EOL}<!-- HeaderEnd -->${EOL}`;

      let expectedString = `<head>${EOL}`;
      expectedString += `<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>${EOL}`;
      expectedString += `<title>TITLE</title>${EOL}`;
      expectedString += `<link href="test.css" rel="stylesheet">${EOL}`;
      expectedString += `</head>${EOL}`;
      // act
      const headString: string = await getHeaders.getHtmlHead(headersString, headers);
      // assert
      expect(headString).toStrictEqual(expectedString);
    });
  });
});
