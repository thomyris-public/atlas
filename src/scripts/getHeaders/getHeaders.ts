import { EOL } from 'os';

export interface GetHeadersInterface {
  getHtmlHead(headersString: string, headers: string[]): string;
}

export class GetHeaders implements GetHeadersInterface {
  getHtmlHead(headersString: string, headers: string[]): string {
    const title = this.findTitle(headersString);
    if (title !== '') {
      const defaultTitleId: number = headers.findIndex((elmt) => elmt.includes('<title>'));
      headers[defaultTitleId] = title;
    }

    let htmlHeader = `<head>${EOL}`;

    for (let i = 0; i < headers.length; i++) {
      const header = headers[i];
      htmlHeader += `${header}${EOL}`;
    }
    htmlHeader += `</head>${EOL}`;

    return htmlHeader;
  }

  private findTitle(headersString: string) {
    const match = headersString.match(/\<!-- title(\n|\r\n){0,1}.*(\n|\r\n){0,1}-->/);
    const title = match ? match[0].split(/\n|\r\n/)[1] : null;

    if (title) {
      const fileTitle = `<title>${title}</title>`;
      return fileTitle;
    }
    return '';
  }
}
