import { promises } from 'fs';

import { GetBreadcrumb } from './getBreadcrumb';
import { Conf } from '../../helpers/getConf/conf.model';
import { GetConf } from '../../helpers/getConf/getConf';

describe('GetBreadcrumb', () => {
  const getConf = new GetConf();
  const getBreadcrumb = new GetBreadcrumb(getConf);

  const spyConf = jest.spyOn(getConf, 'get');
  const spyReaddir = jest.spyOn(promises, 'readdir');
  const spyReadFile = jest.spyOn(promises, 'readFile');

  const conf: Conf = {
    input: 'input',
    output: 'output',
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('#get', () => {
    it('Should generate a breadcrumb for a file at the root dir with a specified title', async () => {
      // arrange
      spyConf.mockResolvedValueOnce(conf);
      spyReaddir.mockResolvedValueOnce([]);
      // act
      const result: string = await getBreadcrumb.get('input/test.md');
      // assert
      expect(result).toStrictEqual('<p><span>input</span></p>');
    });

    it('Should generate a breadcrumb for a file in a subDir', async () => {
      // arrange
      const fileString = `<!-- HeaderBegin -->
<!-- title
title
-->
<!-- HeaderEnd -->`;

      spyConf.mockResolvedValueOnce(conf);
      spyReaddir.mockResolvedValueOnce([]); // current dir
      spyReaddir.mockResolvedValueOnce(['index.md' as any]); // parent dir
      spyReadFile.mockResolvedValueOnce(fileString); // read the index.md file in the parent dir
      // act
      const result: string = await getBreadcrumb.get('input/sub/test.md');
      // assert
      expect(result).toStrictEqual('<p><a href = "..\\index.html">title</a> > <span>sub</span></p>');
    });

    it('Should generate a breadcrumb for a file in a subDir without a specified title', async () => {
      // arrange
      const fileString = '';
      spyConf.mockResolvedValueOnce(conf);
      spyReaddir.mockResolvedValueOnce([]); // current dir
      spyReaddir.mockResolvedValueOnce(['index.md' as any]); // parent dir
      spyReadFile.mockResolvedValueOnce(fileString); // read the index.md file in the parent dir
      // act
      const result: string = await getBreadcrumb.get('input/sub/test.md');
      // assert
      expect(result).toStrictEqual('<p><a href = "..\\index.html">input</a> > <span>sub</span></p>');
    });

    it('Should generate a breadcrumb for a file in a subDir for html index', async () => {
      // arrange
      const fileString = '<title>TEST</title>';
      spyConf.mockResolvedValueOnce(conf);
      spyReaddir.mockResolvedValueOnce([]); // current dir
      spyReaddir.mockResolvedValueOnce(['index.html' as any]); // parent dir
      spyReadFile.mockResolvedValueOnce(fileString); // read the index.md file in the parent dir
      // act
      const result: string = await getBreadcrumb.get('input/sub/test.md');
      // assert
      expect(result).toStrictEqual('<p><a href = "..\\index.html">TEST</a> > <span>sub</span></p>');
    });

    it('Should generate a breadcrumb for a file in a subDir for html index that dont have a title', async () => {
      // arrange
      const fileString = '';
      spyConf.mockResolvedValueOnce(conf);
      spyReaddir.mockResolvedValueOnce([]); // current dir
      spyReaddir.mockResolvedValueOnce(['index.html' as any]); // parent dir
      spyReadFile.mockResolvedValueOnce(fileString); // read the index.md file in the parent dir
      // act
      const result: string = await getBreadcrumb.get('input/sub/test.md');
      // assert
      expect(result).toStrictEqual('<p><a href = "..\\index.html">input</a> > <span>sub</span></p>');
    });
  });
});
