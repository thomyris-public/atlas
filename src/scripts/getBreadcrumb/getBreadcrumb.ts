import { promises } from 'fs';
import { dirname, basename, join, relative, extname } from 'path';

import { Conf } from '../../helpers/getConf/conf.model';
import { GetConfInterface } from '../../helpers/getConf/getConf';

export interface GetBreadcrumbInterface {
  get(outputElmtPath: string): Promise<string>;
}

export class GetBreadcrumb implements GetBreadcrumbInterface {
  private getConf: GetConfInterface;

  constructor(getConf: GetConfInterface) {
    this.getConf = getConf;
  }

  async get(inputElmtPath: string): Promise<string> {
    const conf: Conf = await this.getConf.get();
    let path: string = inputElmtPath;

    type breadCrumbLink = {
      path: string;
      linkContent: string;
    };

    let links: breadCrumbLink[] = [];

    // generate all the links from the file to the root
    while (basename(path) !== conf.input) {
      const parentDirPath = dirname(path);

      const parentFolder = await promises.readdir(parentDirPath);
      path = parentDirPath;
      const file = parentFolder.find((elmt) => elmt === 'index.md' || elmt === 'index.html');

      if (file) {
        const indexPath = join(path, file);
        const linkPath = relative(inputElmtPath, indexPath)
          .replace(/..(\\|\/)/, '')
          .replace('.md', '.html');

        const fileString = await promises.readFile(indexPath, 'utf-8');
        const dirName = basename(dirname(indexPath));

        const title = this.getTitle(fileString, dirName, extname(file));

        const link: breadCrumbLink = {
          path: linkPath,
          linkContent: title,
        };

        links.push(link);
      } else {
        const indexPath = join(path, parentDirPath);
        const fileName = basename(indexPath);

        const title = this.getTitle('', fileName, '');

        const link: breadCrumbLink = {
          path: '',
          linkContent: title,
        };
        links.push(link);
      }
    }

    links = links.reverse();
    let breadCrumbString = '';

    for (let i = 0; i < links.length; i++) {
      if (i === 0) breadCrumbString += '<p>';
      const link = links[i];

      if (link.path === '') {
        breadCrumbString += `<span>${link.linkContent}</span>`;
      } else breadCrumbString += `<a href = "${link.path}">${link.linkContent}</a>`;
      if (i !== links.length - 1) breadCrumbString += ' > ';
      if (i === links.length - 1) breadCrumbString += '</p>';
    }

    return breadCrumbString;
  }

  private getTitle(fileString: string, dirName: string, ext: string): string {
    let title: string | null = null;

    if (ext === '.md') {
      const matchHeader = fileString.match(/\<!-- HeaderBegin \-\-\>((\n|\r\n).*)*<!-- HeaderEnd -->/);

      let fileHeaders = '';

      // get the header string and remove it from the file string
      if (matchHeader) {
        fileHeaders = matchHeader[0];
      }

      const match = fileHeaders.match(/\<!-- title(\n|\r\n){0,1}.*(\n|\r\n){0,1}-->/);
      title = match ? match[0].split(/\n|\r\n/)[1] : null;
    }

    if (ext === '.html') {
      const matchTitle = fileString.match(/\<title>(\n|\r\n){0,}.*(\n|\r\n){0,}<\/title>/);
      title = matchTitle ? matchTitle[0].split(/\<title>/)[1].split(/\<\/title>/)[0] : null;
    }

    let fileTitle = '';
    if (title) {
      fileTitle = title;
    } else {
      fileTitle = dirName;
    }
    return fileTitle;
  }
}
