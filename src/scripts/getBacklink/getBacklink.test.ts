import { promises } from 'fs';
import { GetBacklink } from './getBackLink';

describe('GetBacklink', () => {
  const getBackLink = new GetBacklink();

  const spyReaddir = jest.spyOn(promises, 'readdir');

  describe('#get', () => {
    it('Should return a backlink', async () => {
      // arrange
      spyReaddir.mockResolvedValueOnce([]);
      // act
      const result = await getBackLink.get('/test.md');
      // assert
      expect(result).toStrictEqual('');
    });

    it('Should return a backlink', async () => {
      // arrange
      spyReaddir.mockResolvedValueOnce(['index.md' as any]);
      // act
      const result = await getBackLink.get('input/test.md');
      // assert
      expect(result).toStrictEqual('<a id="backlink" href="index.html">Retour</a>');
    });

    it('Should return a backlink', async () => {
      // arrange
      spyReaddir.mockResolvedValueOnce(['index.md' as any]);
      // act
      const result = await getBackLink.get('input/test.md');
      // assert
      expect(result).toStrictEqual('<a id="backlink" href="index.html">Retour</a>');
    });
  });
});
