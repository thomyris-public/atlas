import { promises } from 'fs';
import { basename, dirname, join, relative } from 'path';

export interface GetBacklinkInterface {
  get(inputElmtPath: string): Promise<string>;
}

export class GetBacklink implements GetBacklinkInterface {
  async get(inputElmtPath: string): Promise<string> {
    const path: string = inputElmtPath;
    const baseName: string = basename(path);

    const parentDirPath: string = baseName === 'index.html' || baseName === 'index.md' ? dirname(dirname(path)) : dirname(path);

    const parentFolder = await promises.readdir(parentDirPath);
    const fileExist = parentFolder.find((elmt) => elmt === 'index.md' || elmt === 'index.html');

    if (fileExist) {
      const indexPath = join(parentDirPath, fileExist);

      const linkPath = relative(inputElmtPath, indexPath)
        .replace(/..(\\|\/)/, '')
        .replace('.md', '.html');

      const backlink = `<a id="backlink" href="${linkPath}">Retour</a>`;
      return backlink;
    }
    return '';
  }
}
