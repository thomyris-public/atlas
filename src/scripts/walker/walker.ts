import * as path from 'path';
import { promises } from 'fs';

import { CreateHtmlFileInterface } from '../createHtmlFile/createHtmlFile';
export interface WalkerInterface {
  walk(inputPath: string, outputPath: string): Promise<void>;
}
export class Walker {
  private createHtmlFile: CreateHtmlFileInterface;

  constructor(createHtmlFile: CreateHtmlFileInterface) {
    this.createHtmlFile = createHtmlFile;
  }

  async walk(inputPath: string, outputPath: string): Promise<void> {
    const dir = await promises.readdir(inputPath);

    for (let i = 0; i < dir.length; i++) {
      const dirElmt = dir[i];
      const elmtPath = path.resolve(inputPath, dirElmt);

      const stat = await promises.stat(elmtPath);

      if (stat.isDirectory()) {
        const outputElmtPath = path.join(outputPath, dirElmt);
        await promises.mkdir(outputElmtPath);
        await this.walk(elmtPath, outputElmtPath);
      }

      if (stat.isFile()) {
        const outputElmtPath = path.join(outputPath, dirElmt);

        if (path.extname(dirElmt) === '.md') {
          const fileString: string = await promises.readFile(elmtPath, 'utf-8');

          // generate the file
          const headers: string[] = ['<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>'];
          const fileTitle = `<title>${dirElmt.replace(/.md/, '')}</title>`;

          const cssPath: string = path.relative(outputPath, 'index.css').replace(/..(\\|\/)/, '');
          const baseCssLink = `<link href="${cssPath}" rel="stylesheet">`;

          const jsPath: string = path.relative(outputPath, 'index.js').replace(/..(\\|\/)/, '');
          const baseJsLink = `<script src="${jsPath}"></script>`;

          headers.push(fileTitle);
          headers.push(baseCssLink);
          headers.push(baseJsLink);
          const htmlFile: string = await this.createHtmlFile.create(fileString, headers, elmtPath);

          await promises.writeFile(outputElmtPath.replace(/.md$/, '.html'), htmlFile);
        } else {
          const fileString = await promises.readFile(elmtPath);
          await promises.writeFile(outputElmtPath, fileString);
        }
      }
    }
  }
}
