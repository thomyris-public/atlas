import { EOL } from 'os';
import { promises, Stats } from 'fs';

import { Walker } from './walker';
import { GetLayout } from '../getLayout/getLayout';
import { GetHeaders } from '../getHeaders/getHeaders';
import { GetConf } from '../../helpers/getConf/getConf';
import { GetBacklink } from '../getBacklink/getBackLink';
import { GetBreadcrumb } from '../getBreadcrumb/getBreadcrumb';
import { FileConverter } from '../fileConverter/fileConverter';
import { CreateHtmlFile } from '../createHtmlFile/createHtmlFile';

describe('Walker', () => {
  const getConf = new GetConf();
  const fileConverter = new FileConverter();
  const getHeaders = new GetHeaders();
  const getLayout = new GetLayout();
  const getBreadcrumb = new GetBreadcrumb(getConf);
  const getBackLink = new GetBacklink();

  const createHtmlFile = new CreateHtmlFile(fileConverter, getHeaders, getLayout, getBreadcrumb, getBackLink);
  const walker = new Walker(createHtmlFile);

  const spyWalk = jest.spyOn(walker, 'walk');
  const spyCreate = jest.spyOn(createHtmlFile, 'create');
  const spyMkdir = jest.spyOn(promises, 'mkdir');
  const spyReaddir = jest.spyOn(promises, 'readdir');
  const spyStat = jest.spyOn(promises, 'stat');
  const spyReadFile = jest.spyOn(promises, 'readFile');
  const spyWriteFile = jest.spyOn(promises, 'writeFile');
  const spyBread = jest.spyOn(getBreadcrumb, 'get');
  const spyBacklink = jest.spyOn(getBackLink, 'get');

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('#walk', () => {
    const baseStat: Stats = {
      isFile: () => false,
      isBlockDevice: () => false,
      isCharacterDevice: () => false,
      isDirectory: () => false,
      isFIFO: () => false,
      isSocket: () => false,
      isSymbolicLink: () => false,
      atime: new Date(0),
      atimeMs: 0,
      birthtime: new Date(0),
      birthtimeMs: 0,
      blksize: 0,
      blocks: 0,
      ctime: new Date(0),
      ctimeMs: 0,
      dev: 0,
      gid: 0,
      ino: 0,
      mode: 0,
      mtime: new Date(0),
      mtimeMs: 0,
      nlink: 0,
      rdev: 0,
      size: 0,
      uid: 0,
    };

    it('Should call convertMdToHtml from FileConverter one time if only one file found', async () => {
      // arrange
      const stat: Stats = { ...baseStat, isFile: () => true };
      spyReaddir.mockResolvedValueOnce(['oneFile.md' as any]); // read the dir
      spyStat.mockResolvedValueOnce(stat);
      spyReadFile.mockResolvedValueOnce(''); // read the file content
      spyWriteFile.mockResolvedValueOnce();
      spyBread.mockResolvedValueOnce('');
      spyBacklink.mockResolvedValueOnce('');
      // act
      await walker.walk('input', 'output');
      // assert
      expect(spyCreate).toHaveBeenCalledTimes(1);
    });

    it('Should call convertMdToHtml from FileConverter one time if only one file found with header param', async () => {
      // arrange
      const file = `<!-- HeaderBegin -->${EOL}<!-- title${EOL}Super titre de tata${EOL}-->${EOL}<!-- HeaderEnd -->${EOL} file content`;

      const stat: Stats = { ...baseStat, isFile: () => true };
      spyReaddir.mockResolvedValueOnce(['oneFile.md' as any]); // read the dir
      spyStat.mockResolvedValueOnce(stat);
      spyReadFile.mockResolvedValueOnce(file); // read the file content
      spyWriteFile.mockResolvedValueOnce();
      spyBread.mockResolvedValueOnce('');
      spyBacklink.mockResolvedValueOnce('');
      // act
      await walker.walk('input', 'output');
      // assert
      expect(spyCreate).toHaveBeenCalledTimes(1);
    });

    it('Should not call convertMdToHtml from FileConverter if the file is not a .md', async () => {
      // arrange
      const stat: Stats = { ...baseStat, isFile: () => true };
      spyReaddir.mockResolvedValueOnce(['oneFile.html' as any]); // read the dir
      spyStat.mockResolvedValueOnce(stat);
      spyReadFile.mockResolvedValueOnce(''); // read the file content
      spyWriteFile.mockResolvedValueOnce();
      spyBread.mockResolvedValueOnce('');
      spyBacklink.mockResolvedValueOnce('');
      // act
      await walker.walk('input', 'output');
      // assert
      expect(spyCreate).not.toHaveBeenCalled();
    });

    it('Should recall walk if found a directory', async () => {
      // arrange
      const stat: Stats = { ...baseStat, isDirectory: () => true };
      const stat2: Stats = { ...baseStat, isFile: () => true };
      spyReaddir.mockResolvedValueOnce(['dir' as any]); // read the dir
      spyStat.mockResolvedValueOnce(stat);
      spyMkdir.mockResolvedValueOnce('mocked'); // create the directory in the output
      spyReaddir.mockResolvedValueOnce(['test.md' as any]); // read the dir
      spyStat.mockResolvedValueOnce(stat2);
      spyReadFile.mockResolvedValueOnce(''); // read the file content
      spyWriteFile.mockResolvedValueOnce();
      spyBread.mockResolvedValueOnce('');
      spyBacklink.mockResolvedValueOnce('');
      // act
      await walker.walk('input', 'output');
      // assert
      expect(spyWalk).toHaveBeenCalledTimes(2);
    });
  });
});
