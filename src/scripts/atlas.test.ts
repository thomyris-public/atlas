import * as fs from 'fs';
const promises = fs.promises;

import { Atlas } from './atlas';
import { Walker } from './walker/walker';
import { GetLayout } from './getLayout/getLayout';
import { Logger } from '../helpers/logger/logger';
import { GetConf } from '../helpers/getConf/getConf';
import { GetHeaders } from './getHeaders/getHeaders';
import { GetBacklink } from './getBacklink/getBackLink';
import { GetBreadcrumb } from './getBreadcrumb/getBreadcrumb';
import { FileConverter } from './fileConverter/fileConverter';
import { CreateHtmlFile } from './createHtmlFile/createHtmlFile';
import { ErrorHandler } from '../helpers/errorHandler/errorHandler';

describe('Atlas', () => {
  const logger = new Logger();
  const getConf = new GetConf();
  const errorHandler = new ErrorHandler(logger);

  const fileConverter = new FileConverter();
  const getHeaders = new GetHeaders();
  const getLayout = new GetLayout();
  const getBreadcrumb = new GetBreadcrumb(getConf);
  const getBackLink = new GetBacklink();
  const createHtmlFile = new CreateHtmlFile(fileConverter, getHeaders, getLayout, getBreadcrumb, getBackLink);
  const walker = new Walker(createHtmlFile);

  const atlas = new Atlas(walker, getConf, logger, errorHandler);

  const spyExist = jest.spyOn(fs, 'existsSync');
  const spyRm = jest.spyOn(promises, 'rm');
  const spyMkdir = jest.spyOn(promises, 'mkdir');
  const spyWriteFile = jest.spyOn(promises, 'writeFile');

  const spyConf = jest.spyOn(getConf, 'get');
  const spyWalk = jest.spyOn(walker, 'walk');
  const spyLogInfo = jest.spyOn(logger, 'info');
  const spyError = jest.spyOn(errorHandler, 'execute');

  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    jest.clearAllMocks();
  });

  describe('#execute', () => {
    it('Should call convert only one time', async () => {
      // arrange
      spyConf.mockResolvedValueOnce({ input: 'inputTest', output: 'testDir' }); // mock the getConf call
      spyExist.mockReturnValueOnce(true);
      spyRm.mockResolvedValueOnce(); // delete the directory in the output
      spyMkdir.mockResolvedValueOnce('testDir'); // create the directory in the output
      spyWriteFile.mockResolvedValueOnce(); // mock create base css file
      spyWriteFile.mockResolvedValueOnce(); // mock create base js file
      spyWalk.mockResolvedValueOnce(); // mock walk call
      spyLogInfo.mockReturnValueOnce(); // mock the log call
      // act
      await atlas.execute();
      // assert
      expect(spyConf).toHaveBeenCalledTimes(1);
      expect(spyRm).toHaveBeenCalledTimes(1);
      expect(spyMkdir).toHaveBeenCalledTimes(1);
      expect(spyWriteFile).toHaveBeenCalledTimes(2); // baseCss and baseJs
      expect(spyWalk).toHaveBeenCalledTimes(1);
      expect(spyLogInfo).toHaveBeenNthCalledWith(1, 'Files converted');
    });

    it('Should call errorHandler if a nerror occur', async () => {
      // arrange
      spyConf.mockRejectedValueOnce('error'); // mock the getConf call
      spyError.mockReturnValueOnce();
      // act
      await atlas.execute();
      // assert
      expect(spyError).toHaveBeenCalledTimes(1);
    });
  });
});
