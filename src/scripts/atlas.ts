import { join } from 'path';
import { promises, watch, existsSync } from 'fs';

import { WalkerInterface } from './walker/walker';
import { LoggerInterface } from '../helpers/logger/logger';
import { GetConfInterface } from '../helpers/getConf/getConf';
import { ErrorHandlerInterface } from '../helpers/errorHandler/errorHandler';

export interface AtlasInterface {
  execute(): Promise<void>;
}

export class Atlas implements AtlasInterface {
  private walker: WalkerInterface;
  private getConf: GetConfInterface;
  private logger: LoggerInterface;
  private errorHandler: ErrorHandlerInterface;

  constructor(walker: WalkerInterface, getConf: GetConfInterface, logger: LoggerInterface, errorHandler: ErrorHandlerInterface) {
    this.walker = walker;
    this.getConf = getConf;
    this.logger = logger;
    this.errorHandler = errorHandler;
  }

  async execute(): Promise<void> {
    try {
      const { input, output } = await this.getConf.get();

      // convert the files
      await this.convert(input, output);

      const isWatchMode = process.argv.slice(2).find((elmt) => elmt === '--watch');

      // watch mode
      /* istanbul ignore next */
      if (isWatchMode) {
        this.logger.info('Watching for files change');
        let savedDate: Date = new Date(0);

        // detect file change in the input dir
        watch(input, { recursive: true, encoding: 'utf-8' }, async () => {
          const date: Date = new Date();
          const timeBetween: number = date.getTime() - savedDate.getTime();
          savedDate = date;

          // 1 second delay between each convertion
          if (timeBetween > 1000) {
            await this.convert(input, output);
          }
        });
      }
    } catch (error) {
      this.errorHandler.execute(error);
    }
  }

  private async convert(input: string, output: string): Promise<void> {
    // recreate the output dir
    if (existsSync(output)) await promises.rm(output, { recursive: true });
    await promises.mkdir(output);
    const baseCssPath = join(output, 'index.css');
    const baseCss = await promises.readFile(join(__dirname, '../assets/base.css'));

    const baseJsPath = join(output, 'index.js');
    const baseJs = await promises.readFile(join(__dirname, '../assets/base.js'));

    await promises.writeFile(baseCssPath, baseCss);
    await promises.writeFile(baseJsPath, baseJs);

    await this.walker.walk(input, output);
    this.logger.info('Files converted');
  }
}
