import { CreateHtmlFile } from './createHtmlFile';
import { GetLayout } from '../getLayout/getLayout';
import { GetHeaders } from '../getHeaders/getHeaders';
import { GetConf } from '../../helpers/getConf/getConf';
import { GetBacklink } from '../getBacklink/getBackLink';
import { GetBreadcrumb } from '../getBreadcrumb/getBreadcrumb';
import { FileConverter } from '../fileConverter/fileConverter';
import { file, fileLayout, fileLayoutNoHeaderTitle, headers, htmlFile, htmlFileLayout, htmlFileLayoutNoHeaderTitle } from './createHtmlFile.test.data';

describe('CreateHtmlFile', () => {
  const getConf = new GetConf();
  const fileConverter = new FileConverter();

  const getHeaders = new GetHeaders();
  const getLayout = new GetLayout();
  const getBreadcrumb = new GetBreadcrumb(getConf);
  const getBackLink = new GetBacklink();
  const createHtmlFile = new CreateHtmlFile(fileConverter, getHeaders, getLayout, getBreadcrumb, getBackLink);

  const spyBreadcrumb = jest.spyOn(getBreadcrumb, 'get');

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('#create', () => {
    it('Should return the html file with the default layout', async () => {
      // arrange
      spyBreadcrumb.mockResolvedValueOnce('<p></p>');
      // act
      const result = await createHtmlFile.create(file, headers, '');
      //assert
      expect(result).toStrictEqual(htmlFile);
    });

    it('Should return an html file with a layout', async () => {
      // arrange
      spyBreadcrumb.mockResolvedValueOnce('<p></p>');
      // act
      const result = await createHtmlFile.create(fileLayout, headers, '');
      //assert
      expect(result).toStrictEqual(htmlFileLayout);
    });

    it('Should return an html file with a layout without a specified title for the header', async () => {
      // arrange
      spyBreadcrumb.mockResolvedValueOnce('<p></p>');
      // act
      const result = await createHtmlFile.create(fileLayoutNoHeaderTitle, headers, '');
      //assert
      expect(result).toStrictEqual(htmlFileLayoutNoHeaderTitle);
    });
  });
});
