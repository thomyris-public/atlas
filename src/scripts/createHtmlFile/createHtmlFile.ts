import { EOL } from 'os';

import { GetLayoutInterface } from '../getLayout/getLayout.js';
import { GetHeadersInterface } from '../getHeaders/getHeaders.js';
import { GetBacklinkInterface } from '../getBacklink/getBackLink.js';
import { FileConverterInterface } from '../fileConverter/fileConverter.js';
import { GetBreadcrumbInterface } from '../getBreadcrumb/getBreadcrumb.js';

export interface CreateHtmlFileInterface {
  create(fileString: string, headers: string[], inputElmtPath: string): Promise<string>;
}

export class CreateHtmlFile {
  private fileConverter: FileConverterInterface;
  private getHeaders: GetHeadersInterface;
  private getLayout: GetLayoutInterface;
  private getBreadcrumb: GetBreadcrumbInterface;
  private getBacklink: GetBacklinkInterface;

  constructor(
    fileConverter: FileConverterInterface,
    getHeaders: GetHeadersInterface,
    getLayout: GetLayoutInterface,
    getBreadcrumb: GetBreadcrumbInterface,
    getBacklink: GetBacklinkInterface,
  ) {
    this.fileConverter = fileConverter;
    this.getHeaders = getHeaders;
    this.getLayout = getLayout;
    this.getBreadcrumb = getBreadcrumb;
    this.getBacklink = getBacklink;
  }

  async create(fileString: string, headers: string[], inputElmtPath: string): Promise<string> {
    const match = fileString.match(/\<!-- HeaderBegin \-\-\>((\n|\r\n).*)*<!-- HeaderEnd -->/);

    let fileHeaders = '';

    // get the header string and remove it from the file string
    if (match) {
      fileHeaders = match[0];
      fileString = fileString.slice(match.index! + match[0].length);
    }

    // generate all blocks
    const headersString: string = this.getHeaders.getHtmlHead(fileHeaders, headers);
    const convertedFile: string = this.fileConverter.convertMdToHtml(fileString);
    const layout: string = this.getLayout.getHtmlLayout(fileHeaders);
    const breadcrumb: string = await this.getBreadcrumb.get(inputElmtPath);
    const backlink: string = await this.getBacklink.get(inputElmtPath);

    const header = this.createHeader(layout, headers);
    const menu = this.createMenu(layout);

    // replace header md syntax with the generated blocks
    let htmlLayout = layout.replace(/<line>/g, '<div class = "line">').replace(/<\/line>/g, '</div>' + EOL);
    htmlLayout = htmlLayout.replace(/<block>/g, '<div class = "block">').replace(/<\/block>/g, '</div>' + EOL);
    htmlLayout = htmlLayout.replace(/<header( [^>]*)?>/, header);
    htmlLayout = htmlLayout.replace(/<content>/, `<div>${convertedFile}</div>`);
    htmlLayout = htmlLayout.replace(/<breadcrumb>/, `<div>${breadcrumb}</div>`);
    htmlLayout = htmlLayout.replace(/<backlink>/, `<div>${backlink}</div>`);
    htmlLayout = htmlLayout.replace(/<menu>/, menu);

    const htmlFile = `<!DOCTYPE html>${EOL}<html>${EOL}${headersString}${EOL}<body>${EOL}${htmlLayout}${EOL}</body>${EOL}</html>`;

    return htmlFile;
  }

  private createHeader(layout: string, headers: string[]): string {
    const match = layout.match(/<header( [^>]*)?>/);
    if (match) {
      const splitedHeader = match[0].split(/title=/);
      let title = '';
      if (splitedHeader.length > 1) title = splitedHeader[1].replace(/"|>/g, '');
      else {
        const defaultTitle: string | undefined = headers.find((elmt) => elmt.includes('<title>'));
        if (defaultTitle) title = defaultTitle.replace(/<title>|<\/title>/g, '');
      }
      return `${EOL}<header>${EOL}<h1>${title}</h1>${EOL}</header>${EOL}`;
    }
    return '';
  }

  private createMenu(layout: string): string {
    const match = layout.match(/<menu( [^>]*)?>/);
    if (match) {
      return `<div id = "menu"></div>${EOL}`;
    }
    return '';
  }
}
