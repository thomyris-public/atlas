import { EOL } from 'os';

const headers = ['<title>test</title>', '<link href="path" rel="stylesheet">'];

const file = `<!-- HeaderBegin -->${EOL}<!-- title${EOL}test${EOL}-->${EOL}<!-- HeaderEnd -->${EOL}file content`;

let htmlFile = `<!DOCTYPE html>${EOL}`;
htmlFile += `<html>${EOL}`;
htmlFile += `<head>${EOL}`;
htmlFile += `<title>test</title>${EOL}`;
htmlFile += `<link href=\"path\" rel=\"stylesheet\">${EOL}`;
htmlFile += `</head>${EOL}${EOL}`;
htmlFile += `<body>${EOL}`;
htmlFile += `    <div class = \"block\">${EOL}`;
htmlFile += `<header>${EOL}`;
htmlFile += `<h1>test</h1>${EOL}`;
htmlFile += `</header>${EOL}`;
htmlFile += `</div>${EOL}${EOL}`;
htmlFile += `    <div class = \"line\">${EOL}`;
htmlFile += `      <div id = "menu"></div>${EOL}${EOL}`;
htmlFile += `      <div class = \"block\">${EOL}`;
htmlFile += `        <div><p></p></div>${EOL}`;
htmlFile += `        <div>  <p>file content</p>${EOL}${EOL}`;
htmlFile += `</div>${EOL}`;
htmlFile += `        <div></div>${EOL}`;
htmlFile += `      </div>${EOL}${EOL}`;
htmlFile += `    </div>${EOL}${EOL}`;
htmlFile += `</body>${EOL}`;
htmlFile += '</html>';

const fileLayout = `<!-- HeaderBegin -->${EOL}<!-- layout${EOL}<block><content></block>${EOL}--><!-- HeaderEnd -->${EOL}file content`;

let htmlFileLayout = `<!DOCTYPE html>${EOL}`;
htmlFileLayout += `<html>${EOL}`;
htmlFileLayout += `<head>${EOL}`;
htmlFileLayout += `<title>test</title>${EOL}`;
htmlFileLayout += `<link href=\"path\" rel=\"stylesheet\">${EOL}`;
htmlFileLayout += `</head>${EOL}${EOL}`;
htmlFileLayout += `<body>${EOL}`;
htmlFileLayout += `<div class = \"block\"><div>  <p>file content</p>${EOL}${EOL}`;
htmlFileLayout += `</div></div>${EOL}${EOL}`;
htmlFileLayout += `</body>${EOL}`;
htmlFileLayout += '</html>';

const fileLayoutNoHeaderTitle = `<!-- HeaderBegin -->${EOL}<!-- layout${EOL}<block><header title="test"></block>${EOL}--><!-- HeaderEnd -->${EOL}file content`;

let htmlFileLayoutNoHeaderTitle = `<!DOCTYPE html>${EOL}`;
htmlFileLayoutNoHeaderTitle += `<html>${EOL}`;
htmlFileLayoutNoHeaderTitle += `<head>${EOL}`;
htmlFileLayoutNoHeaderTitle += `<title>test</title>${EOL}`;
htmlFileLayoutNoHeaderTitle += `<link href=\"path\" rel=\"stylesheet\">${EOL}`;
htmlFileLayoutNoHeaderTitle += `</head>${EOL}${EOL}`;
htmlFileLayoutNoHeaderTitle += `<body>${EOL}`;
htmlFileLayoutNoHeaderTitle += `<div class = \"block\">${EOL}`;
htmlFileLayoutNoHeaderTitle += `<header>${EOL}`;
htmlFileLayoutNoHeaderTitle += `<h1>test</h1>${EOL}`;
htmlFileLayoutNoHeaderTitle += `</header>${EOL}`;
htmlFileLayoutNoHeaderTitle += `</div>${EOL}${EOL}`;
htmlFileLayoutNoHeaderTitle += `</body>${EOL}`;
htmlFileLayoutNoHeaderTitle += '</html>';

export { headers, file, htmlFile, fileLayout, htmlFileLayout, fileLayoutNoHeaderTitle, htmlFileLayoutNoHeaderTitle };
