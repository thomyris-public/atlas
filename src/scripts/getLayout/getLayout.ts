import { EOL } from 'os';
export interface GetLayoutInterface {
  getHtmlLayout(headersString: string): string;
}

export class GetLayout implements GetLayoutInterface {
  getHtmlLayout(headersString: string): string {
    return this.readLayout(headersString);
  }

  private readLayout(headersString: string) {
    const match = headersString.match(/<!-- layout(\n|\r\n)(.+(?!-->)(\n|\r\n))+-->/);
    const defaultLayout = `<!-- layout
    <block><header></block>
    <line>
      <menu>
      <block>
        <breadcrumb>
        <content>
        <backlink>
      </block>
    </line>
-->`;

    const layout = match ? match[0] : defaultLayout;

    // remove first and last comment line
    const splited = layout.split(/\n|\r\n/);
    splited.pop(); // <!-- layout
    splited.shift(); // -->

    return splited.join(EOL);
  }
}
