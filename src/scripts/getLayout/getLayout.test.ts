import { EOL } from 'os';

import { GetLayout } from './getLayout';

describe('GetLayout', () => {
  const getLayout = new GetLayout();

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('Should return the layout', async () => {
    // arrange
    const headersString = `<!-- HeaderBegin -->${EOL}<!-- layout${EOL}<block><navbar title="test"></block>${EOL}-->${EOL}<!-- HeaderEnd -->${EOL}`;
    // act
    const layout: string = await getLayout.getHtmlLayout(headersString);
    // assert
    expect(layout).toStrictEqual('<block><navbar title="test"></block>');
  });

  it('Should return the default layout', async () => {
    // arrange
    const headersString = `<!-- HeaderBegin -->${EOL}<!-- HeaderEnd -->${EOL}`;
    let defaultLayout = `    <block><header></block>${EOL}`;
    defaultLayout += `    <line>${EOL}`;
    defaultLayout += `      <menu>${EOL}`;
    defaultLayout += `      <block>${EOL}`;
    defaultLayout += `        <breadcrumb>${EOL}`;
    defaultLayout += `        <content>${EOL}`;
    defaultLayout += `        <backlink>${EOL}`;
    defaultLayout += `      </block>${EOL}`;
    defaultLayout += '    </line>';
    // act
    const layout: string = await getLayout.getHtmlLayout(headersString);
    // assert
    expect(layout).toStrictEqual(defaultLayout);
  });
});
