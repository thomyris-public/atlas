[![npm version](https://img.shields.io/npm/v/atlas_md.svg)](https://www.npmjs.com/package/atlas_md)
# Atlas 

Atlas convert markdown to html


## Prerequisite

Done with node version `16.13.1`.

## Install

Run `npm i atlas_md`.
## Usage

Add a script in your `package.json` that run `atlas_md`.

Create a json file for the configuration.

Add the path to your configuration file in the script.
Script `atlas_md conf.json` or add it after your script call `npm run atlas conf.json`.

The generated files are located in your "output" directory.

## Configuration file

The file must be writen as a json and have the properties:
- input: **string**
- output: **string**
### Example
```json
{
  "input": "input",
  "output": "output"
}
```

## Headers

You can specify some things using commnet at the begining of a markdown file.

All headers must be in this block.

```md
<!-- HeaderBegin -->

<!-- HeaderEnd -->
```

### Page title

Will give the title `Cool title` to the page.

```md
<!-- HeaderBegin -->

<!-- title
Cool title
-->
<!-- HeaderEnd -->
```

### Page layout

Will define your page layout.

Default layout below
```md
<!-- HeaderBegin -->

<!-- title
Hello i am a title
-->

<!-- layout
<block><header title="test"></block>
<line>
  <menu>
  <block>
    <breadcrumb>
    <content>
 <backlink>
  </block>
</line>
-->
<!-- HeaderEnd -->
```

#### Layout's Blocks

`<header>` is the file's header that display an `h1` with the `title` arg or the file name.

`<menu>` is a table of content listing all the file's headings.

`<breadcrumb>` is a breadcrumb from the file to the root. Generate a link to a `index.html` or `index.md` file in each folder present in the path to the root.

`<content>` The file content

`<backlink>` A link to the nearest `index` file.
## Syntax

### Headings

markdown

```markdown
# Heading level 1
## Heading level 2
### Custom Id{#custom-id}
```

### Font style

markdown

```markdown
**bold** text

__bold text 2__

*italic text*

_italic text 2_

***bold and italic***

___bold and italic 2___

__*bold and italic 3*__

**_bold and italic 4_**

~~The world is flat.~~

~~_**The world is flat.**_~~

==very important words==
```

### Straigth line 

markdown

```markdown
***

---

_______________________________________________________________
```

### Link

Use only the displayed string to link to a Header

markdown

```markdown
[link with tooltip](https://www.markdownguide.org/basic-syntax/#links "tooltip")

[Heading IDs](#heading-level-1)
```

### Image

markdown

```markdown
![image](image/path "San Juan Mountains")

![](image/path){title: 'my title'}

# Tooltip replace the title attribute
![image](image/path){title: 'my title'}

# Tooltip replace the alt attribute
![image](image/path "tooltip")

# Tooltip replace the alt & title attributes
![image](image/path "tooltip"){title: 'my title'}

# The first value for the size is the height and the second is the width
![image](image/path "San Juan Mountains"){title: 'my title', size: '800x400'}

# Align the items on the row
# Value: center or left or right
![image](image/path "San Juan Mountains"){title: 'my title', size: '500x600', align: 'left'}

[![image with link](image/path "super phrase de test du tooltip")](https://www.markdownguide.org/basic-syntax/#links)
```

### Inline code

markdown

```markdown
inline `code`
```

### Block code

markdown

````markdown
```
block code
```
````

### Block quote

markdown

````markdown
>Block quote first line
>> Block inside a block
> second Line
````

### Ordered list

markdown

````markdown
1. First item
2. Second item
3. Third item
    1. sub list
4. Fourth item

or 

1. First item
8. Second item
3. Third item
    1. sub list
5. Fourth item 
````

### Unordered list

markdown

````markdown

- First item
- Second item
- Third item
    - sub list
- Fourth item 

or 

* First item
* Second item
* Third item
    * sub list
* Fourth item 

or 

+ First item
+ Second item
+ Third item
    + sub list
+ Fourth item 
````

### Check list

markdown

````markdown
- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media
````

### Footnote

Footnote will always be at file end

markdown

````markdown
A footnote link[^1]
[^1]: The first footnote.
````

### Table

Define the column text alignment with the second line

```markdown
--- = default left
:--- = left
---: = right
:---: = center
```

markdown

````markdown
| text left     | text center | text right |
| :---------- | :-------: | --------: |
| content      |  content    |
````

### Definition list

markdown

````markdown
First Term
: First term definition

Second Term
: Second term definition
: Second term definition second line.
````